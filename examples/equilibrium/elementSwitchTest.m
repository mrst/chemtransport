% clear;
% close all;

%% load ad and chemtransport modules
mrstModule add chemtransport ad-core
% mrstVerbose on

%% generate chemical system object
% here we look at a simple example of aqueous speciation  of sodium,
% protons, and chlorine

elements = {'O*', 'H*', 'Na*', 'Cl*'};

species = {'H+', 'OH-', 'Na+', 'Cl-', 'NaCl', 'H2O'};

reactions = {'H2O  = H+  + OH- ', 10^-14*mol/litre, ...
             'NaCl = Na+ + Cl-',  10^1*mol/litre};

chemsys   = ChemicalSystem(elements, species, reactions);
chemmodel = AqueousReactionModel(chemsys);

%% We set up the input model

O   = [0; 1; 1];
H   = [0; 2; 2];
Na  = [1; 0; 0];
Cl  = [1; 0; 0];

elements = [O, H, Na, Cl]*mol/litre;

elementNames = chemsys.elementNames;
inputmodel = InputModel(chemsys, elementNames, elements);

%% We set up the solver model

model = AqueousReactionInputModel(chemmodel, inputmodel);

%% We set up the initial guess

nI = size(elements, 1);

presentElements = (elements > 0);
elements(~presentElements) = 0;
logElements = log(elements);
logElements(~presentElements) = NaN;

state = [];
state = model.setProp(state, 'presentElements', presentElements);
state = model.setProp(state, 'elements'       , elements);
state = model.setProp(state, 'logElements'    , logElements);

% default starting values
nC = chemsys.nC;
state.species    = 0*ones(nI, nC);
state.logSpecies = log(state.species);
state.presentSpecies = false(nI, nC);

state = model.applyPhysicalConstraints(state);

% We udpate the primary variables from the state.
state = model.syncPrimaryFromState(state);

%% solve the chemical system
state = model.solveChemicalState(state);

%% Use initState function
state2  = chemmodel.initState(elements);


% <html>
% <p><font size="-1">
% Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.
% </font></p>
% <p><font size="-1">
% This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).
% </font></p>
% <p><font size="-1">
% MRST is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% </font></p>
% <p><font size="-1">
% MRST is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% </font></p>
% <p><font size="-1">
% You should have received a copy of the GNU General Public License
% along with MRST.  If not, see
% <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses</a>.
% </font></p>
% </html>
