%% Simple chemistry example
% 
% This example computes the chemical equilibrium for the system given below
% (see structures elements, species and reactions)
%
% The input are given using the parsing convention: a star (*) after the
% component name assigns the component as an input (see chemsys.inputNames for
% the name of the inputs)
%
% This example is the same as in the geochemistry module. Since the solver is
% set up differently in this module, we use this example to detail how the
% system of equations is set up using an auxiliary InputModel. At the end, we
% use the function AqueousReactionModel.initState to obtain the same result,
% without going throug all the setup (which is done internally). 
%
% At the end, we also include a computation where charge balance is included.
%
% see also calcite.m example

%% load ad and chemtransport modules
mrstModule add ad-core geochemistry chemtransport 
mrstVerbose on

%% generate chemical system object
% here we look at a simple example of aqueous speciation  of sodium,
% protons, and chlorine

elements = {'O', 'H', 'Na*', 'Cl*'};

species = {'H+*', 'OH-', 'Na+', 'Cl-', 'NaCl', 'H2O*'};

reactions = {'H2O  = H+  + OH- ', 10^-14*mol/litre, ...
             'NaCl = Na+ + Cl-',  10^1*mol/litre};

chemsys   = ChemicalSystem(elements, species, reactions);
chemmodel = AqueousReactionModel(chemsys);


%% We set up the input model

n = 10;

Na  = ones(n,1)*milli*mol/litre;
Cl  = ones(n,1)*milli*mol/litre;
H2O = ones(n,1)*mol/litre;
H   = logspace(-4, -10, n)'*mol/litre;

inputValues = [Na, Cl, H, H2O];
inputNames = chemsys.inputNames;

inputmodel = InputLogModel(chemsys, inputNames, inputValues);

%% We set up the solver model
model = AqueousReactionInputModel(chemmodel, inputmodel);

%% We set up the initial guess

nI = size(inputValues, 1);
state = [];

% default starting values
nMC = chemsys.nMC;
nC = chemsys.nC;
elements    = 1*ones(nI, nMC)*mol/litre;
logElements = log(elements);
species     = 1*ones(nI, nC)*mol/litre;
logSpecies  = log(species);

state = model.setProp(state, 'elements'   , elements);
state = model.setProp(state, 'logElements', logElements);
state = model.setProp(state, 'species'    , species);
state = model.setProp(state, 'logSpecies' , logSpecies);

% We change the default values such that they fulfill the natural bounds.
varsToBound = {'species'};
state = applyNaturalBounds(chemmodel, varsToBound, state);

% we update the present Species from the present elements content.  We need
% first to create the fields presentSpecies. We use default values.
state.presentElements = true(nI, nMC);
state.presentSpecies  = true(nI, nC);
state = model.syncPresentFlagsFromState(state);

% We udpate the primary variables from the state.
state = model.syncPrimaryFromState(state);

%% We solve the chemical system
[state, failure] = model.solveChemicalState(state);

%% The computation done above can be obtained by simply running the command initState
state = chemmodel.initState(inputValues);

%% We enforce charge balance
state = chemmodel.initState(inputValues, 'chargeBalance', 'Na');

%% process the data

state = changeUnits(state, {'elements','species'}, mol/litre );
pH = -log10(chemmodel.getProp(state, 'H+'));

%% plot the results
figure; hold on; box on;
plot(pH, state.species, 'linewidth',2)

xlabel('pH')
ylabel('concentration [mol/L]');
set(gca, 'yscale', 'log');
legend(chemsys.speciesNames)




% <html>
% <p><font size="-1">
% Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.
% </font></p>
% <p><font size="-1">
% This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).
% </font></p>
% <p><font size="-1">
% MRST is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% </font></p>
% <p><font size="-1">
% MRST is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% </font></p>
% <p><font size="-1">
% You should have received a copy of the GNU General Public License
% along with MRST.  If not, see
% <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses</a>.
% </font></p>
% </html>
