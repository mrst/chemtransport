%% load ad and chemtransport modules
mrstModule add chemtransport ad-core

%% generate chemical system object
% here we look at a simple example of aqueous speciation  of sodium,
% protons, and chlorine

elements = {'O*', 'H*', 'Ca*', 'CO3*', 'Na*', 'Cl*'};

species = {'H+', 'OH-',  'H2O', 'Ca+2', 'CO3-2', 'HCO3-', 'H2CO3', 'CaCO3(s)', ...
          'Na+', 'Cl-', 'NaCl'};

reactions ={'H2O        = H+  + OH- '  , 10^-14*(mol/litre)    , ...
            'CaCO3(s)   = CO3-2 + Ca+2', 10^-8.48*(mol/litre)^2, ...
            'CO3-2 + H+ = HCO3-'       , 10^10.329/(mol/litre) , ...
            'HCO3- + H+ = H2CO3'       , 10^6.352/(mol/litre)  , ...
            'NaCl = Na+ + Cl-'         , 10^1*mol/litre};

chemsys   = ChemicalSystem(elements, species, reactions);
chemmodel = DissPrecModel(chemsys);


%% define the input 
O   = [0   ; 1    ];
H   = [0   ; 2    ];
Ca  = [0   ; 1e-3 ];
CO3 = [0   ; 1e-3 ];
Na  = [1e-3; 0    ];
Cl  = [1e-3; 0    ];

elements = [O, H, Ca, CO3, Na, Cl]*mol/litre;

elementNames = chemsys.elementNames;
inputmodel = InputModel(chemsys, elementNames, elements);

%% We set up the solver model
model = DissPrecInputModel(chemmodel, inputmodel);

%% We set up the initial guess

nI = size(elements, 1);

presentElements = (elements > 0);
elements(~presentElements) = 0;
logElements = log(elements);
logElements(~presentElements) = NaN;

state = [];
state = model.setProp(state, 'presentElements', presentElements);
state = model.setProp(state, 'elements'       , elements);
state = model.setProp(state, 'logElements'    , logElements);

% default starting values
nC = chemsys.nC;
nS = chemsys.nS;

species    = 1*ones(nI, nC)*mol/litre;
logSpecies = log(species);
solidSpecies    = 1*ones(nI, nS)*mol/litre;
logSolidSpecies = log(solidSpecies);

state = model.setProp(state, 'species'        , species);
state = model.setProp(state, 'logSpecies'     , logSpecies);
state = model.setProp(state, 'solidSpecies'   , solidSpecies);
state = model.setProp(state, 'logSolidSpecies', logSolidSpecies);

state.presentSpecies = true(nI, nC);
state.presentSolids  = true(nI, nS);


% We change the default values such that they fulfill the natural bounds.
state = model.applyPhysicalConstraints(state);
% We udpate the primary variables from the state.
state = model.syncPrimaryFromState(state);

%% solve the chemical system
[state1, failure] = model.solveChemicalState(state);

%% Use initState function
state2  = chemmodel.initState(elements);

%% Use chemmodel solveChemicalState function
elements = state.elements;
elementNames = chemsys.elementNames;
state3 = chemmodel.solveChemicalState(elementNames, elements, state, ...
                                      'inputModelType', 'log');



% <html>
% <p><font size="-1">
% Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.
% </font></p>
% <p><font size="-1">
% This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).
% </font></p>
% <p><font size="-1">
% MRST is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% </font></p>
% <p><font size="-1">
% MRST is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% </font></p>
% <p><font size="-1">
% You should have received a copy of the GNU General Public License
% along with MRST.  If not, see
% <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses</a>.
% </font></p>
% </html>
