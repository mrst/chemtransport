
%% load adi and chemtransport module
mrstModule add ad-core geochemistry chemtransport 

%% instantiate the chemical system 
% here we look at carbonate speciation with calcite and CO2(g) as non
% aquoeus phases. We choose the partial pressure of CO2 as an input by
% appending an asterisk to the name

elements = {'O', 'H', 'Ca*', 'CO3*'};

species = {'H+*', 'OH-',  'H2O*', 'Ca+2', 'CO3-2', 'HCO3-', 'H2CO3', 'CaCO3(s)'};

reactions ={'H2O        = H+  + OH- '  , 10^-14*(mol/litre)    , ...
            'CaCO3(s)   = CO3-2 + Ca+2', 10^-8.48*(mol/litre)^2, ...
            'CO3-2 + H+ = HCO3-'       , 10^10.329/(mol/litre) , ...
            'HCO3- + H+ = H2CO3'       , 10^6.352/(mol/litre)};



chemsys = ChemicalSystem(elements, species, reactions);

chemmodel = DissPrecModel(chemsys);

chemmodel.printChemicalSystem;

%% set on debumode to get plot Newton iterations
chemmodel.debugmode = false;

%% specify inputs
% here we vary the partial pressure of CO2 from 1e-3 atm to 3 atm

n = 100;
Ca  = 1e-3*ones(n, 1)*mol/litre;
CO3 = 1e-1*ones(n, 1)*mol/litre;
H2O = ones(n, 1)*mol/litre;
H   = logspace(-4, -14, n)'*mol/litre;

state = chemmodel.initState([Ca, CO3, H, H2O]);


%% plot the results
figure;
hold on;

pH = -log10(getProp(chemmodel, state, 'H+'));
plot(pH, state.species, 'linewidth', 2)
plot(pH, state.solidSpecies, 'linewidth', 2)
xlabel('pH')
ylabel('concentration [mol/L]');
set(gca, 'yscale', 'log');
legend({chemsys.speciesNames{:}, chemsys.solidNames{:}})

figure
hold on
ca2p = chemmodel.getProp(state, 'Ca+2');
caco3s = chemmodel.getProp(state, 'CaCO3(s)');
plot(pH, ca2p, 'linewidth', 2)
plot(pH, caco3s, 'linewidth', 2)
xlabel('pH')
ylabel('concentration [mol/L]');
legend({'Ca2+', 'CaCO3(s)'});
title('Dissolution of calcite');



% <html>
% <p><font size="-1">
% Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.
% </font></p>
% <p><font size="-1">
% This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).
% </font></p>
% <p><font size="-1">
% MRST is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% </font></p>
% <p><font size="-1">
% MRST is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% </font></p>
% <p><font size="-1">
% You should have received a copy of the GNU General Public License
% along with MRST.  If not, see
% <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses</a>.
% </font></p>
% </html>
