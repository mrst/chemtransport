clear all;

%% add the necessary modules
mrstModule add ad-core ad-props ad-blackoil chemtransport geochemistry mrst-gui
% mrstVerbose off

%% Define the domain
% here we look at a 2D cartesian grid  with 100 cells

nx = 30; ny = 30;
G = cartGrid([nx, ny], [1, 1]);
G = computeGeometry(G);
nc = G.cells.num;

%% Define the rock
% porosity and permeability are the minimum number of parameters for rock

rock.perm = 1*darcy*ones(G.cells.num, 1);
rock.poro = 0.5*ones(G.cells.num, 1);

%% Define the fluid
pRef = 0*barsa;
fluid = initSimpleADIFluid('phases', 'W', 'mu', 1*centi*poise, 'rho', ...
                           1000*kilogram/meter^3, 'c', 1e-10, 'cR', 4e-10, ...
                           'pRef', pRef);
% we consider an incompressible fluid
fluid.bW = @(p) (1 + 0*p);

%% Define the chemistry
% here we look at a simple tracer chemical system

elements = {'O', 'H', 'Na*', 'Cl'};

species = {'H+*', 'OH-', 'Na+', 'H2O*', 'NaCl', 'Cl-*'};

reactions ={'H2O  = H+  + OH- ',      10^-14*mol/litre, ...
            'NaCl = Na+ + Cl-',       10^1*mol/litre};


% instantiate chemical model
chemsys   = ChemicalSystem(elements, species, reactions);
chemmodel = AqueousReactionModel(chemsys);

%% solve the chemistry for the initial and injected compositions
% we will be injected a low slainity fluid into a high salinity aquifer. we
% solve the system so that we can give the total element concentrations
% within the domain at the initial time, and define the boundary conditions

% initial chemistry
NaT = 1e-2;
H   = 1e-7;
H2O = 1;
Cl  = H; % therefore we get charge balance

initial = [NaT H H2O Cl]*mol/litre;

% we must repeat the initial chemistry for each cell of the system. This
% can be done by passing a vector to initState, or repmatting the state
% variable produced
[state0, initreport]= chemmodel.initState(repmat(initial, nc, 1));


% the initial state must also contain a pressure field
state0.pressure          = pRef*ones(nc,1);

% injected chemistry
NaT = 1e-3;
H   = 1e-7;
H2O = 1;
Cl  = H; % therefore we get charge balance
injected = [NaT H H2O Cl]*mol/litre;
[injChemState, injreport]= chemmodel.initState(injected);

% return

%% We set up the boundary condition for this 1d model

dirCell     = G.cells.num;
dirPressure = 100*barsa;

influxcells = 1;
influxrates = 1*meter^3/day;
influxConc  = chemmodel.getProp(injChemState, 'Elements');
influxConc  = mat2cell(influxConc, 1, ones(numel(influxConc), 1));

bc = struct('dirCell'    , dirCell    , ...
            'dirPressure', dirPressure, ...
            'influxcells', influxcells, ...
            'influxrates', influxrates);
bc.influxConc = influxConc;


%% Define the transport model

chemmodel  = AqueousReactionModel(chemsys);
transmodel = TransportLogModel(G, rock, fluid, bc, chemsys);

model = AqueousReactionTransportModel(chemmodel, transmodel);

assert(model.checkModel(), 'Model is not correctly set up.');

%% Define the time stepping schedule

steps    = [1e-3*ones(10, 1); 1e-2*day*ones(10, 1)];
schedule = simpleSchedule(steps);


%% Run the simulation

[~, states, scheduleReport] = simulateScheduleAD(state0, model, schedule);


%% visualize the simulation
clf
plotToolbar(G, states, 'field', 'logSpecies:5')

%% Copyright notice
%
% <html>
% <p><font size="-1">
% Copyright 2009-2017 SINTEF ICT, Applied Mathematics.
% </font></p>
% <p><font size="-1">
% This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).
% </font></p>
% <p><font size="-1">
% MRST is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% </font></p>
% <p><font size="-1">
% MRST is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% </font></p>
% <p><font size="-1">

% You should have received a copy of the GNU General Public License
% along with MRST.  If not, see
% <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses</a>.
% </font></p>
% </html>



% <html>
% <p><font size="-1">
% Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.
% </font></p>
% <p><font size="-1">
% This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).
% </font></p>
% <p><font size="-1">
% MRST is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% </font></p>
% <p><font size="-1">
% MRST is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% </font></p>
% <p><font size="-1">
% You should have received a copy of the GNU General Public License
% along with MRST.  If not, see
% <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses</a>.
% </font></p>
% </html>
