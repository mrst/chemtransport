%% add the necessary modules
mrstModule add ad-core ad-props ad-blackoil chemtransport geochemistry mrst-gui

% Permeability reduction parameter
calciteMax = 1e-3*mol/litre;
% calciteMax = inf;
calciteRegTerm = 1e-3;
% Scenario 'dissolution' or 'precipitation'
scenario = 'dissolution'; 
% Solver option: 
solvercase = 'log transport';
% Verbosity option: 
chemVerbose = false;
chemDebug   = false;
transportVerbose = true;
% Tolerance of nonlinear internal chemical solver
chemnonlinearTolerance = 1e-2;


%% Define the domain
% here we look at a 2D cartesian grid  with 100 cells

nx = 30; ny = 30;
G = cartGrid([nx, ny], [1, 1]);
G  = computeGeometry(G);
nc = G.cells.num;

%% Define the rock
% porosity and permeability are the minimum number of parameters for rock

rock.perm = 1*darcy*ones(G.cells.num, 1);
rock.poro = 0.5*ones(G.cells.num, 1);

%% Define the fluid
pRef = 0*barsa;
fluid = initSimpleADIFluid('phases', 'W', 'mu', 1*centi*poise, 'rho', ...
                           1000*kilogram/meter^3, 'c', 1e-10, 'cR', 4e-10, ...
                           'pRef', pRef);
% we consider an incompressible fluid
fluid.bW = @(p) (1 + 0*p);
fluid.permredfunction = @(calcite) permredfunction(calcite, calciteMax, calciteRegTerm);

%% Define the chemistry

elements = {'O', 'H', 'Ca*', 'CO3*'};

species = {'H+*', 'OH-', 'H2O*', 'Ca+2', 'CO3-2', 'HCO3-', 'H2CO3', 'CaCO3(s)'};

reactions = {'H2O        = H+  + OH- '  , 10^-14*(mol/litre)    , ...
             'CaCO3(s)   = CO3-2 + Ca+2', 10^-8.48*(mol/litre)^2, ...
             'CO3-2 + H+ = HCO3-'       , 10^10.329/(mol/litre) , ...
             'HCO3- + H+ = H2CO3'       , 10^6.352/(mol/litre)};

% instantiate chemical model
chemsys   = ChemicalSystem(elements, species, reactions);
chemmodel = DissPrecModel(chemsys);
chemmodel.verbose = chemVerbose;
chemmodel.debugmode = chemDebug;
chemmodel.nonlinearTolerance = chemnonlinearTolerance;

%% solve the chemistry for the initial and injected compositions
% we will be injected a low slainity fluid into a high salinity aquifer. we
% solve the system so that we can give the total element concentrations
% within the domain at the initial time, and define the boundary conditions

switch scenario
  case 'dissolution'
    Ca  = 1e-2;
    CO3 = 1e-2;
    H   = 1e-12;
    H2O = 1;
    initial = [Ca CO3 H H2O]*mol/litre;

    Ca  = 1e-5;
    CO3 = 1e-5;
    H   = 1e-2;
    H2O = 1;
    injected = [Ca CO3 H H2O]*mol/litre;
    
  case 'precipitation'
    Ca  = 1e-5;
    CO3 = 1e-5;
    H   = 1e-2;
    H2O = 1;
    initial = [Ca CO3 H H2O]*mol/litre;

    Ca  = 1e-2;
    CO3 = 1e-2;
    H   = 1e-12;
    H2O = 1;
    injected = [Ca CO3 H H2O]*mol/litre;
  otherwise 
    error('scenario not recognized');
end

fprintf('*** Set up initial state\n');

% Set up the initial state. We start with the chemical composition

stateinit = chemmodel.initState(initial);


%% Setup init state using the previously computed /single cell/ state

nMC = chemsys.nMC;
nC  = chemsys.nC;
nS  = chemsys.nS;

stateinit.presentElements = true(1, nMC);
stateinit.presentSpecies  = true(1, nC);
stateinit.presentSolids   = true(1, nS);

clear state0
fds = fieldnames(stateinit);
for i = 1 : numel(fds)
    fd = fds{i};
    val = stateinit.(fd);
    state0.(fd) = repmat(val, nc, 1);
end

% the initial state0 must also contain a pressure field
state0.pressure = pRef*ones(nc,1);

% Set up the injected chemistry
fprintf('*** Compute injection composition\n');
injChemState = chemmodel.initState(injected);


%% We set up the boundary condition for this 1d model

dirCell     = G.cells.num;
dirPressure = 100*barsa;

influxcells = 1;
influxrates = 1*meter^3/day;
influxConc  = chemmodel.getProp(injChemState, 'Elements');
influxConc  = mat2cell(influxConc, 1, ones(numel(influxConc), 1));

bc = struct('dirCell'    , dirCell    , ...
            'dirPressure', dirPressure, ...
            'influxcells', influxcells, ...
            'influxrates', influxrates);
bc.influxConc = influxConc;

pressuremodel = CalcitePressureModel(G, rock, fluid, bc, chemsys);

%% Define the transport model

switch solvercase
  case 'log transport'
    transmodel = CalciteTransportLogModel(G, rock, fluid, bc, chemsys);
    model      = CalciteDissPrecPressureTransportModel(chemmodel, transmodel, ...
                                                      pressuremodel);
  otherwise
    error('solvercase not recognized');
end
[valid, str] = model.checkModel();
assert(valid, 'Model is not correctly set up.');

% update all the flags and set up primary variables
state0 = model.syncPresentFlagsFromState(state0);
state0 = model.syncPrimaryFromState(state0);


%% Define the time stepping schedule
time = 1e-1*day;
dt = 1e-3*day;
dtvec = rampupTimesteps(time, dt, 8);
dtvec = dtvec(1 : end - 1);
schedule = simpleSchedule(dtvec);

%% Set up debug and verbose flag
model.debugmode = false;
model.verbose = transportVerbose;

model.exactChemSolveIterations = inf;
chemmodel.nonlinearTolerance = 1e-2;


%% Run the simulation

fprintf('*** Transport simulation\n');
solver = NonLinearSolver('maxIterations', 40);
[~, states, scheduleReport] = simulateScheduleAD(state0, model, schedule, ...
                                                 'NonLinearSolver', solver);

%% visualize the simulation
clf
plotToolbar(G, states, 'field', 'solidSpecies')

%% Copyright notice
%
% <html>
% <p><font size="-1">
% Copyright 2009-2017 SINTEF ICT, Applied Mathematics.
% </font></p>
% <p><font size="-1">
% This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).
% </font></p>
% <p><font size="-1">
% MRST is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% </font></p>
% <p><font size="-1">
% MRST is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% </font></p>
% <p><font size="-1">

% You should have received a copy of the GNU General Public License
% along with MRST.  If not, see
% <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses</a>.
% </font></p>
% </html>



% <html>
% <p><font size="-1">
% Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.
% </font></p>
% <p><font size="-1">
% This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).
% </font></p>
% <p><font size="-1">
% MRST is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% </font></p>
% <p><font size="-1">
% MRST is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% </font></p>
% <p><font size="-1">
% You should have received a copy of the GNU General Public License
% along with MRST.  If not, see
% <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses</a>.
% </font></p>
% </html>
