
%% add the necessary modules
mrstModule add ad-core ad-props ad-blackoil chemtransport geochemistry mrst-gui
mrstVerbose off

%% Define the domain
% here we look at a 2D cartesian grid  with 100 cells

nx = 10;
G  = cartGrid([nx, 1], [1, 1]);
G  = computeGeometry(G);
nc = G.cells.num;

%% Define the rock
% porosity and permeability are the minimum number of parameters for rock

rock.perm = 1*darcy*ones(G.cells.num, 1);
rock.poro = 0.5*ones(G.cells.num, 1);

%% Define the fluid
pRef = 0*barsa;
fluid = initSimpleADIFluid('phases', 'W', 'mu', 1*centi*poise, 'rho', ...
                           1000*kilogram/meter^3, 'c', 1e-10, 'cR', 4e-10, ...
                           'pRef', pRef);
% we consider an incompressible fluid
fluid.bW = @(p) (1 + 0*p);

%% Define the chemistry

elements = {'O', 'H', 'Na*', 'Cl*', 'Bo*'};

species = {'H+*', 'OH-', 'Na+', 'Cl-', 'NaCl', 'H2O*', 'BoH', 'Bo-'};

reactions = {'H2O  = H+  + OH- ', 10^-14*mol/litre, ...
             'NaCl = Na+ + Cl-' , 10^1*mol/litre  , ...
             'BoH = H+ + Bo-'   , 1.3028e-09*mol/litre};

% instantiate chemical model
chemsys   = ChemicalSystem(elements, species, reactions);


%% solve the chemistry for the initial and injected compositions
% we will be injected a low slainity fluid into a high salinity aquifer. we
% solve the system so that we can give the total element concentrations
% within the domain at the initial time, and define the boundary conditions

fprintf('*** Set up initial state\n');
chemmodel = AqueousReactionModel(chemsys);

%% We set up the input model to compute the initial composition
Na  = 1e-1;
Cl  = 1e-1;
Bo  = 1e-1;
H   = 1e-12;
H2O = 1;

initial = [Na Cl Bo H H2O]*mol/litre;

state0 = chemmodel.initState(initial);

%% Set up splitted region

nC = chemsys.nC;
nMC = chemsys.nMC;

state1 = state0;
state1 = chemmodel.setProp(state1, 'Bo' , 0);
state1 = chemmodel.setProp(state1, 'Bo-', 0);
state1 = chemmodel.setProp(state1, 'BoH', 0);
state1.presentSpecies  = true(1, nC);
state1.presentElements = true(1, nMC);

state2 = state0;
state2 = chemmodel.setProp(state2, 'Na+' , 0);
state2 = chemmodel.setProp(state2, 'Cl-' , 0);
state2 = chemmodel.setProp(state2, 'NaCl', 0);
state2 = chemmodel.setProp(state2, 'Na'  , 0);
state2 = chemmodel.setProp(state2, 'Cl'  , 0);
state2.presentSpecies  = true(1, nC);
state2.presentElements = true(1, nMC);

N1 = floor(nc/2);
N2 = nc - N1;

clear state
fds = {'species', 'elements', 'logSpecies', 'logElements', ...
       'presentSpecies', 'presentElements'};
for i = 1 : numel(fds)
    fd = fds{i};
    val1 = state1.(fd);
    val2 = state2.(fd);
    state.(fd) = [repmat(val1, N1, 1); repmat(val2, N2, 1)];
end

% the initial state must also contain a pressure field
state.pressure = pRef*ones(nc,1);

state0 = state;

%% We set up the boundary condition for this 1d model

dirCell     = G.cells.num;
dirPressure = 100*barsa;

influxcells = 1;
influxrates = 1*meter^3/day;
influxConc  = chemmodel.getProp(state1, 'Elements');
influxConc  = mat2cell(influxConc, 1, ones(numel(influxConc), 1));

bc = struct('dirCell'    , dirCell    , ...
            'dirPressure', dirPressure, ...
            'influxcells', influxcells, ...
            'influxrates', influxrates);
bc.influxConc = influxConc;


%% Define the transport model

transmodel = TransportLogModel(G, rock, fluid, bc, chemsys);

model = AqueousReactionTransportModel(chemmodel, transmodel);

[valid, str] = model.checkModel();
assert(valid, 'Model is not correctly set up.');

% update all the flags and set up primary variables
state0 = model.syncPresentFlagsFromState(state0);
state0 = model.syncPrimaryFromState(state0);


%% Define the time stepping schedule
steps    = [1e-1*ones(5, 1); 1e2*ones(10, 1); 1e3*ones(5, 1); 1e7*ones(30, 1)];
schedule = simpleSchedule(steps);

%% Set debug mode
model.debugmode = true;

%% Run the simulation
fprintf('*** Transport simulation\n');
[~, states, scheduleReport] = simulateScheduleAD(state0, model, schedule);
 

%% visualize the simulation
clf
plotToolbar(G, states, 'plot1d', true, 'field', 'Elements:1')




% <html>
% <p><font size="-1">
% Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.
% </font></p>
% <p><font size="-1">
% This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).
% </font></p>
% <p><font size="-1">
% MRST is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% </font></p>
% <p><font size="-1">
% MRST is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% </font></p>
% <p><font size="-1">
% You should have received a copy of the GNU General Public License
% along with MRST.  If not, see
% <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses</a>.
% </font></p>
% </html>
