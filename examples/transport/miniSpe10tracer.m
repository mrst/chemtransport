%% add the necessary modules
mrstModule add ad-core ad-props ad-blackoil chemtransport geochemistry mrst-gui spe10 incomp postprocessing


%% Extract subgrid from layer 9 of SPE10

layer = 9;
[Gl, W, rockl] = getSPE10setup(layer);

Gl = computeGeometry(Gl);
x = Gl.cells.centroids(:, 1);
indx = (x > 150) & (x < 250);
y = Gl.cells.centroids(:, 2);
indy = (y > 360) & (y < 500);
ind = indx & indy;

[G, gcells] = extractSubgrid(Gl, ind);
rock = [];
rock.perm = rockl.perm(gcells, :);
rock.poro = rockl.poro(gcells, :);

physdim = [];
for i = 1 : 2
    dim = Gl.nodes.coords(:, i);
    dim = sort(dim);
    dim = unique(dim);
    dim = unique(diff(dim));
    physdim(i) = dim(1);
end
cartdim = Gl.cartDims;
nc = Gl.cells.num;
nx = (nnz(indx)/nc)*cartdim(1);
ny = (nnz(indy)/nc)*cartdim(2);

physdim = physdim.*[nx, ny];
G  = cartGrid([nx, ny], physdim);
G  = computeGeometry(G);
nc = G.cells.num;

%% Define the fluid
pRef = 0*barsa;
% Set up fluid
fluid = initSimpleFluid('n'  , [2,2]                      , ...
                        'mu' , [1,5]*centi*poise          , ...
                        'rho', [1000,800]*kilogram/meter^3);
% we consider an incompressible fluid
fluid.bW = @(p) (1 + 0*p);


%% Define wells

W = [];
wellargs = {'Type'  , 'rate'       , ...
            'Val'   , 1e2*meter^3/day, ...
            'Sign'  , 1            , ...
            'Comp_i', [1]       , ...
            'Name'  , 'Inj'};
W = addWell(W, G, rock, 1, wellargs{:});
wellargs = {'Type'  , 'bhp'    , ...
            'Val'   , 100*barsa, ...
            'Sign'  , -1       , ...
            'Comp_i', [1], ...
            'Name'  , 'Prod'};
W = addWell(W, G, rock, nc, wellargs{:});

% Add tracer in well
W(1).tracer = 1;
W(2).tracer = 0;

pMin = W(2).val;

%% Initial state
state0 = initResSol(G, pMin, [1]);


%% compute pressure

T = computeTrans(G, rock);
state = incompTPFA(state0, G, T, fluid, 'wells', W);

%% Define the time stepping schedule

time = 10*day;
dt = 1e-1*day;
dtvec = rampupTimesteps(time, dt, 20);
schedule = simpleSchedule(dtvec, 'W', W);


%% Setup tracer model

tracermodel = TracerModel(G, rock, fluid, 'tracerNames', {'T'});
state0 = tracermodel.setProp(state, 'T', zeros(nc, 1));


%% setup output handler
dataFolder = 'minispe10tracer';
dataDirectory = 'dump';
cleardir = false;
tracerHandler =  ResultHandler('dataPrefix'   , 'state'      , ...
                               'writeToDisk'  , true         ,...
                               'dataDirectory', dataDirectory, ...
                               'dataFolder'   , dataFolder   , ...
                               'cleardir'     , cleardir);

%% Run simulation
fprintf('*** Transport simulation\n');

[~, states, scheduleReport] = simulateScheduleAD(state0, tracermodel, schedule, ...
                                                 'OutputHandler', ...
                                                 tracerHandler');

%% plot results
clf, plotToolbar(G, states, 'pauseTime', 0.01)





% <html>
% <p><font size="-1">
% Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.
% </font></p>
% <p><font size="-1">
% This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).
% </font></p>
% <p><font size="-1">
% MRST is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% </font></p>
% <p><font size="-1">
% MRST is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% </font></p>
% <p><font size="-1">
% You should have received a copy of the GNU General Public License
% along with MRST.  If not, see
% <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses</a>.
% </font></p>
% </html>
