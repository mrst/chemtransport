%% add the necessary modules
mrstModule add ad-core ad-props ad-blackoil chemtransport geochemistry mrst-gui spe10

%% Setting parameters

% Include variable perm
PermType = 'variable'; % 'variable' or 'fixed'
% Permeability reduction parameter
calciteMax     = 1e-3*mol/litre;
calciteRegTerm = 1e-3;
% scenario 'dissolution' or 'precipitation'
scenario = 'dissolution'; 
% solver option: 
solvercase = 'log transport';
% verbosity option: 
debugmode = false;
chemVerbose = false;
transportVerbose = true;
% Solver options
chemnonlinearTolerance = 1e-2;
% Output options
dataDirectory = 'dump';
cleardir = false;
% Other options
doSetup = true;
plotPoroPerm = false;

%% Extract subgrid from layer 9 of SPE10

if doSetup
    layer = 9;
    [Gl, W, rockl] = getSPE10setup(layer);

    Gl = computeGeometry(Gl);
    x = Gl.cells.centroids(:, 1);
    indx = (x > 150) & (x < 250);
    y = Gl.cells.centroids(:, 2);
    indy = (y > 360) & (y < 500);
    ind = indx & indy;
    
    [G, gcells] = extractSubgrid(Gl, ind);
    rock = [];
    rock.perm = rockl.perm(gcells, :);
    rock.poro = rockl.poro(gcells, :);

    physdim = [];
    for i = 1 : 2
        dim = Gl.nodes.coords(:, i);
        dim = sort(dim);
        dim = unique(dim);
        dim = unique(diff(dim));
        physdim(i) = dim(1);
    end
    cartdim = Gl.cartDims;
    nc = Gl.cells.num;
    nx = (nnz(indx)/nc)*cartdim(1);
    ny = (nnz(indy)/nc)*cartdim(2);
    
    physdim = physdim.*[nx, ny];
    G  = cartGrid([nx, ny], physdim);
    G  = computeGeometry(G);
    nc = G.cells.num;
    
    if plotPoroPerm
        figure(1)
        clf
        plotCellData(G, rock.perm(:, 1), 'edgecolor', 'none');
        title('permeability');
        figure(2)
        clf
        plotCellData(G, rock.poro, 'edgecolor', 'none');
        title('porosity');
    end
    
end


%% Define the fluid
pRef = 0*barsa;
fluid = initSimpleADIFluid('phases', 'W', 'mu', 1*centi*poise, 'rho', ...
                           1000*kilogram/meter^3, 'c', 1e-10, 'cR', 4e-10, ...
                           'pRef', pRef);
% we consider an incompressible fluid
fluid.bW = @(p) (1 + 0*p);
fluid.permredfunction = @(calcite) permredfunction(calcite, calciteMax, ...
                                                  calciteRegTerm);

%% Define the chemistry

elements = {'O', 'H', 'Ca*', 'CO3*'};

species = {'H+*', 'OH-',  'H2O*', 'Ca+2', 'CO3-2', 'HCO3-', 'H2CO3', 'CaCO3(s)'};

reactions ={'H2O        = H+  + OH- '  , 10^-14*(mol/litre)    , ...
            'CaCO3(s)   = CO3-2 + Ca+2', 10^-8.48*(mol/litre)^2, ...
            'CO3-2 + H+ = HCO3-'       , 10^10.329/(mol/litre) , ...
            'HCO3- + H+ = H2CO3'       , 10^6.352/(mol/litre)};

% instantiate chemical model
chemsys = ChemicalSystem(elements, species, reactions);
chemmodel = DissPrecModel(chemsys);
chemmodel.verbose = chemVerbose;
chemmodel.nonlinearTolerance = chemnonlinearTolerance;

%% solve the chemistry for the initial and injected compositions
% we will be injected a low slainity fluid into a high salinity aquifer. we
% solve the system so that we can give the total element concentrations
% within the domain at the initial time, and define the boundary conditions

switch scenario
  case 'dissolution'
    Ca  = 1e-2;
    CO3 = 1e-2;
    H   = 1e-12;
    H2O = 1;
    initial = [Ca CO3 H H2O]*mol/litre;

    Ca  = 1e-5;
    CO3 = 1e-5;
    H   = 1e-2;
    H2O = 1;
    injected = [Ca CO3 H H2O]*mol/litre;
    
  case 'precipitation'
    Ca  = 1e-5;
    CO3 = 1e-5;
    H   = 1e-2;
    H2O = 1;
    initial = [Ca CO3 H H2O]*mol/litre;

    Ca  = 1e-2;
    CO3 = 1e-2;
    H   = 1e-12;
    H2O = 1;
    injected = [Ca CO3 H H2O]*mol/litre;
  otherwise 
    error('scenario not recognized');
end

fprintf('*** Set up initial state\n');

% Set up the initial state. We start with the chemical composition

stateinit = chemmodel.initState(initial);


%% Setup init state using the previously computed /single cell/ state

nMC = chemsys.nMC;
nC  = chemsys.nC;
nS  = chemsys.nS;

stateinit.presentElements = true(1, nMC);
stateinit.presentSpecies  = true(1, nC);
stateinit.presentSolids   = true(1, nS);

clear state0
fds = fieldnames(stateinit);
for i = 1 : numel(fds)
    fd = fds{i};
    val = stateinit.(fd);
    state0.(fd) = repmat(val, nc, 1);
end

% the initial state0 must also contain a pressure field
state0.pressure = pRef*ones(nc,1);

% Set up the injected chemistry
fprintf('*** Compute injection composition\n');
injChemState = chemmodel.initState(injected);


%% We set up the boundary condition for this 1d model

dirCell     = G.cells.num;
dirPressure = 100*barsa;

influxcells = 1;
influxrates = 1e2*meter^3/day;
influxConc  = chemmodel.getProp(injChemState, 'Elements');
influxConc  = mat2cell(influxConc, 1, ones(numel(influxConc), 1));

bc = struct('dirCell'    , dirCell    , ...
            'dirPressure', dirPressure, ...
            'influxcells', influxcells, ...
            'influxrates', influxrates);
bc.influxConc = influxConc;

switch PermType
  case 'fixed'
    pressuremodel = PressureModel(G, rock, fluid, bc, chemsys);
  case 'variable'
    pressuremodel = CalcitePressureModel(G, rock, fluid, bc, chemsys);
  otherwise
    error('PermType not recognized.');
end


%% Define the transport model

switch solvercase
  case 'log transport'
    switch PermType
      case 'fixed'
        transmodel = TransportNopressureLogModel(G, rock, fluid, bc, chemsys);
        model      = DissPrecPressureTransportModel(chemmodel, transmodel, pressuremodel);
      case 'variable'
        transmodel = CalciteTransportLogModel(G, rock, fluid, bc, chemsys);
        model      = CalciteDissPrecPressureTransportModel(chemmodel, transmodel, ...
                                                          pressuremodel);
      otherwise
        error('PermType not recognized.');
    end    
  case 'no log transport'
      switch PermType
        case 'fixed'
          warning('not tested');
          transmodel = TransportNopressureModel(G, rock, fluid, bc, chemsys);
          model      = DissPrecPressureTransportModel(chemmodel, transmodel, ...
                                                      pressuremodel);
        case 'variable'
          error('not set up yet');
        otherwise
          error('PermType not recognized.');
      end    
  otherwise
    error('solvercase not recognized');
end
[valid, str] = model.checkModel();
assert(valid, 'Model is not correctly set up.');

% update all the flags and set up primary variables
state0 = model.syncPresentFlagsFromState(state0);
state0 = model.syncPrimaryFromState(state0);


%% Define the time stepping schedule

time = 20*day;
dt = 1e-1*day;
dtvec = rampupTimesteps(time, dt, 20);
schedule = simpleSchedule(dtvec);

%% Set up debug and verbose flag
model.debugmode = debugmode;
model.verbose = transportVerbose;

model.exactChemSolveIterations = inf;


%% setup output handler
makeHandler = @(prefix, dataFolder) ResultHandler('dataPrefix'   , prefix       , ...
                                                  'writeToDisk'  , true         ,...
                                                  'dataDirectory', dataDirectory, ...
                                                  'dataFolder'   , dataFolder   , ...
                                                  'cleardir'     , cleardir);

% dataFolder = sprintf('minispe10calcite_%s_%s', scenario, PermType);
% outputHandler = makeHandler('state', dataFolder);
% state0 = outputHandler{43};
% nstep = numelData(outputHandler);
% steps = schedule.step;
% newsteps.val = steps.val(nstep + 1 : end);
% newsteps.control = steps.control(nstep + 1 : end);
% newschedule.step = newsteps;
% newschedule.control = schedule.control;

time = 10*day;
dt = 1e-1*day;
dtvec = rampupTimesteps(time, dt, 5);
schedule = simpleSchedule(dtvec);

dataFolder = sprintf('minispe10calcite_%s_%s', scenario, PermType);
outputHandler = makeHandler('state', dataFolder);
reportHandler = makeHandler('report', dataFolder);

%% Set up non-linear solver
ts = IterationCountTimeStepSelector('targetIterationCount', 5);
solver = NonLinearSolver('TimeStepSelector', ts, ...
                         'maxIterations', 10);

%% Run simulation
fprintf('*** Transport simulation\n');

[~, states, scheduleReport] = simulateScheduleAD(state0, model, schedule, ...
                                                 'NonLinearSolver', solver, ...
                                                 'OutputHandler', outputHandler, ...
                                                 'ReportHandler', ...
                                                 reportHandler);
%% visualize the simulation
clf
plotToolbar(G, outputHandler, 'field', 'solidSpecies')


% <html>
% <p><font size="-1">
% Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.
% </font></p>
% <p><font size="-1">
% This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).
% </font></p>
% <p><font size="-1">
% MRST is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% </font></p>
% <p><font size="-1">
% MRST is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% </font></p>
% <p><font size="-1">
% You should have received a copy of the GNU General Public License
% along with MRST.  If not, see
% <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses</a>.
% </font></p>
% </html>
