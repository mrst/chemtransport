function debugPlot(model, state)
    
    waitforbutton = false;
    
    if isa(model, 'ChemicalPressureTransportModel')
        chemmodel = model.getSubModel('chemicalModel');
    elseif isa(model, 'ChemicalBaseModel')
        chemmodel = model;
    end
    
    chemsys = chemmodel.chemicalSystem;
    if waitforbutton
        fignum = 1; 
    else
        fignum = 0;
    end
    

    fignum = fignum + 1;
    createorsetfigure(fignum)
    clf
    myplot(state.logElements);
    legend(chemsys.elementNames);
    title('log Elements');
    drawnow
    
    fignum = fignum + 1;
    createorsetfigure(fignum)
    clf
    myplot(state.logSpecies);
    legend(chemsys.speciesNames);
    title('log Species');
    drawnow
    
    if chemsys.nS > 0
        fignum = fignum + 1;
        createorsetfigure(fignum)
        clf
        myplot(state.logSolidSpecies);
        legend(chemsys.solidNames);
        title('log Solid');
        drawnow
        
        fignum = fignum + 1;
        createorsetfigure(fignum)
        clf
        myplot(state.presentSolids);
        legend(chemsys.solidNames);
        title('present solid');
        drawnow
    end
    
    
    if waitforbutton
        figure(1)
        clf
        text(0.5,0.5, 'press on this figure to continue', 'horizontalalignment', 'center');
        waitforbuttonpress
    end

    
end

function createorsetfigure(i)
    figs = get(0, 'Children');
    fignums = arrayfun(@(x) get(x, 'Number'), figs);
    if ismember(i, fignums)
        set(0, 'currentfigure', i);
    else
        figure(i)
    end
end

function myplot(vals)
    hold on
    s = size(vals);
    if s(1) > 1
        plot(vals, '*-');
    else
        for i = 1 : s(2)
            plot(vals(i), '*');
        end
    end
end



%{
Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
