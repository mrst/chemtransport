function [eqs, names, types] = equationsCalciteTransport(state0, stateAD, model, dt, ...
                                                  drivingForces, varargin)

    %% Transport equations for the calcite system, which includes also the
    %% permeability reduction due to Calcite precipitation
    
    opt = struct('Verbose', mrstVerbose, ...
                 'reverseMode', false,...
                 'resOnly', false,...
                 'iteration', -1);  % Compatibility only

    opt = merge_options(opt, varargin{:});

    G  = model.G;
    op = model.operators;
    f  = model.fluid;
    bc = model.bc;

    masterComps0 = model.getADProp(state0, 'Elements');
    masterComps  = model.getADProp(stateAD, 'Elements');
    components   = model.getADProp(stateAD, 'Species');

    p0 = model.getADProp(state0, 'pressure');
    p  = model.getADProp(stateAD, 'pressure');
    
    trans = op.T;

    bW   = f.bW(p);
    bW0  = f.bW(p0);
    mobW = 1./f.muW(p);
    dpW  = op.Grad(p);
    % water upstream-index
    upcw = (value(dpW)<=0);
    vW   = - op.faceUpstr(upcw, mobW).*trans.*dpW;
    bWvW = op.faceAvg(bW).*vW;

    % get calcite concentration
    calcite = model.getADProp(stateAD, 'CaCO3(s)');
    coef = f.permredfunction(calcite);
    coef = op.faceUpstr(upcw, coef);
        
    bWvW = coef.*op.faceAvg(bW).*vW;

    divTerm = op.Div(bWvW);
    
    chemsys   = model.chemicalSystem;
    nMC       = chemsys.nMC;

    eqs   = cell(1, nMC);
    names = cell(1, nMC);
    types = cell(1, nMC);

    
    cellinj = bc.influxcells;
    injflux = bc.influxrates;
    dircell = bc.dirCell;
    
    % Equations of mass conservation for the chemistry
    nC        = chemsys.nC;
    fluidComp = model.fluidComponents;

    fluidConcs = cell(1, nMC);
    for i = 1 : nMC
        fluidConcs{i} = 0;
        for j = 1 : nC
            fluidConcs{i} = fluidConcs{i} + fluidComp(i,j)*components{j};
        end
        accumTerm  = (op.pv/dt).*(masterComps{i}.*bW - masterComps0{i}.*f.bW(p0));
        fluxTerm   = op.Div(op.faceUpstr(upcw, fluidConcs{i}).*bWvW);
        eqs{i} = accumTerm + fluxTerm;
        
        injTerm             = bc.influxConc{i}*injflux;
        eqs{i}(cellinj) = eqs{i}(cellinj) - injTerm;
        
        prodTerm            = -fluidConcs{i}(dircell).*divTerm(dircell);
        eqs{i}(dircell) = eqs{i}(dircell) + prodTerm;
        
        names{i} = sprintf('transport of %s', chemsys.elementNames{i});
    end

    [types{:}] = deal('cell');
    
end




%{
Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
