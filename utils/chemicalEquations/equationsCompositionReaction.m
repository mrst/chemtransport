function [eqs, names, types] = equationsCompositionReaction(model, stateAD)

    chemsys = model.chemicalSystem;

    logComponents         = model.getADProp(stateAD, 'logSpecies');
    logMasterComponents   = model.getADProp(stateAD, 'logElements');
    combinationComponents = model.getADProp(stateAD, 'combinationComponents');
    logPartialPressures   = model.getADProp(stateAD, 'logPartialPressures');
    logSaturationIndicies = model.getADProp(stateAD, 'logSaturationIndicies');

   
    CM = chemsys.compositionMatrix;
    RM = chemsys.reactionMatrix;
    GM = chemsys.gasReactionMatrix;
    SM = chemsys.solidReactionMatrix;
    
    nC = size(CM, 2);
    
    stateAD = model.syncStateADFromPrimary(stateAD);

    components       = model.getADProp(stateAD, 'species');
    masterComponents = model.getADProp(stateAD, 'elements');
    
    logK = chemsys.logReactionConstants;
    
    eqs   = cell(1, chemsys.nMC + chemsys.nR + chemsys.nLC);
    names = cell(1, chemsys.nMC + chemsys.nR + chemsys.nLC);
    types = cell(1, chemsys.nMC + chemsys.nR + chemsys.nLC);

    %% mol fractions
    surfMat = repmat(chemsys.surfMaster, 1, chemsys.nC).*CM;
    surfTest = logical(sum(surfMat));
    
    moleFraction = components;
    
    for i = 1 : chemsys.nC
        if surfTest(i)
            surfDen = 0;
            surfNum = 0;
            for j = 1 : chemsys.nMC
                surfNum = surfNum + CM(j,i).*chemsys.surfMaster(j);
                surfDen = surfDen + value(logical(CM(j,i).*chemsys.surfMaster(j)))*masterComponents{j};
            end
            moleFraction{i} = (surfNum./surfDen).*components{i};
        end

    end
    logMoleFraction = cellfun(@(x) log(x), moleFraction, 'UniformOutput', false);
    
    %% reaction matrix
    for i = 1 : chemsys.nR

        eqs{i} = -logK{i}(:);
        
        for k = 1 : nC
            eqs{i} = eqs{i} + RM(i, k).*logMoleFraction{k};
        end
        
        % gas reactions
        for k = 1 : chemsys.nG
            eqs{i} = eqs{i} + GM(i,k).*logPartialPressures{k};
        end
        
        % solid reactions
        for k = 1 : chemsys.nS
            eqs{i} = eqs{i} + SM(i,k).*logSaturationIndicies{k};
        end
        
        names{i} = chemsys.rxns{i};
    end
    
    %% composition matrix
    for i = 1 : chemsys.nMC
        
        j = chemsys.nR + i;
        masssum = 0;
        
        for k = 1 : nC
            masssum = masssum + CM(i,k).*components{k};
        end

        eqs{j} = logMasterComponents{i} - log(masssum);
                
        names{j} = ['Conservation of ', chemsys.elementNames{i}] ;
    end

    
    %% combination matrix
    for i = 1 : chemsys.nLC
        combeqSumPos = 0;
        combeqSumNeg = 0;
        for k = 1 : chemsys.nC
            coef = chemsys.combinationMatrix(i,k);
            if coef >= 0
                combeqSumPos = combeqSumPos + coef.*components{k};
            else
                combeqSumNeg = combeqSumNeg - coef.*components{k};
            end                
        end

        combComp = combinationComponents{i};
        indpos = (combComp >= 0);
        combeqSumNeg(indpos) = combeqSumNeg(indpos) + combComp(indpos);
        combeqSumPos(~indpos)  = combeqSumPos(~indpos) - combComp(~indpos);
        
        indsmall = (-20 > log(combeqSumNeg)) | (-20 > log(combeqSumPos));
        combeq       = combeqSumPos - combeqSumNeg;
        combeq(~indsmall) = log(combeqSumPos) - log(combeqSumNeg); 
        
        eqs{end+1} = combeq;
        names{end + 1} = [chemsys.combinationNames{i}] ;
        types{end + 1} = 'cell';
    end
    
    
end




%{
Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
