function [eqs, names, types] = equationsAqueousReaction(model, stateAD)

    chemsys = model.chemicalSystem;

    species     = model.getADProp(stateAD, 'species');
    elements    = model.getADProp(stateAD, 'elements');
    logSpecies  = model.getADProp(stateAD, 'logSpecies');
    logElements = model.getADProp(stateAD, 'logElements');


    presentSpecies = model.getADProp(stateAD, 'presentSpecies');
    presentSpecies = horzcat(presentSpecies{:});
    nonpresentSpecies  = ~presentSpecies;

    presentElements = model.getADProp(stateAD, 'presentElements');
    presentElements = horzcat(presentElements{:});
    nonpresentElements  = ~presentElements;

    CM = chemsys.compositionMatrix;
    RM = chemsys.reactionMatrix;

    nMC = chemsys.nMC;
    nC  = chemsys.nC;
    nR  = chemsys.nR;
    nLC = chemsys.nLC;
    assert(nLC == 0, ['this implementation does not handle linear ' ...
                      'combinations']);

    logK = chemsys.logReactionConstants;
    K    = chemsys.reactionConstants;

    nEqs = nR + nMC;
    eqs   = cell(1, nEqs);
    names = cell(1, nEqs);
    types = cell(1, nEqs);

    for i = 1 : nEqs
        eqs{i} = NaN*species{1};
    end

    absRM = abs(RM);

    nonuseLogK = nonpresentSpecies*absRM';
    nonuseLogK = (nonuseLogK > 0);
    useLogK    = ~nonuseLogK;

    [Lmults, Rmults] = setupRMmult(RM, presentSpecies);

    ncell = size(presentSpecies, 1);
    pseudoLogK = cell(1, nR);
    for i = 1 : nR
        pseudoLogK{i} = logK{i}*ones(ncell, 1);
        pseudoLogK{i}(~useLogK(:, i)) = 0;
    end

    pseudoSpecies = cell(1, nC);
    for i = 1 : nC
        pseudoSpecies{i} = logSpecies{i};
        pseudoSpecies{i}(~presentSpecies(:, i)) = species{i}(~presentSpecies(:, i));
    end


    rhs = cell(1, nR);
    lhs = cell(1, nR);
    ind = (1 : ncell)';
    for i = 1 : nR
        lhs{i} = 0*species{1};
        lmult = Lmults(i + (ind - 1)*nR, :);
        for k = 1 : nC
            lhs{i} = lhs{i} + lmult(:, k).*pseudoSpecies{k};
        end

        rhs{i} = 0;
        rmult = Rmults(i + (ind - 1)*nR, :);
        for k = 1 : nR
            rhs{i} = rhs{i} + rmult(:, k).*pseudoLogK{k};
        end

        eqs{i} = lhs{i} - rhs{i};
        names{i} = chemsys.rxns{i};
    end


    %% Mass conservation equations

    % regularization constant
    rc = exp(model.regconstlog);
    for i = 1 : nMC

        j = nR + i;

        masssum = 0*species{1};

        for k = 1 : nC
            if CM(i, k) ~= 0
                masssum = masssum + CM(i, k).*species{k};
            end
        end

        eqs{j}  = elements{i} - masssum;

        logmaster = log(elements{i} + rc);
        logmass   = log(masssum + rc);
        presentElement = presentElements(:, i);
        eqs{j}(presentElement) = logmaster(presentElement) - ...
            logmass(presentElement);

        names{j} = ['Conservation of ', chemsys.elementNames{i}] ;

    end

end



%{
Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
