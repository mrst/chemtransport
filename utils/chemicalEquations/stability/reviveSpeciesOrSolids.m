function state = reviveSpeciesOrSolids(model, state, revivetype)

    switch revivetype
      case 'species'
        doReviveSpecies = true;
        doReviveSolids  = false;
      case 'solids'
        doReviveSpecies = false;
        doReviveSolids  = true;
      otherwise
        error('revivetype not recognized')
    end
    
    species         = model.getProp(state, 'species');
    logSpecies      = model.getProp(state, 'logSpecies');
    presentSpecies  = model.getProp(state, 'presentSpecies');
    if doReviveSolids
        solidSpecies    = model.getProp(state, 'solidSpecies');
        logSolidSpecies = model.getProp(state, 'logSolidSpecies');
        presentSolids   = model.getProp(state, 'presentSolids');    
    end

    logspecval = model.logSpeciesResetValue;
    logsolval  = model.logSolidsResetValue;
        
    ncell = size(species, 1);
    chemsys = model.chemicalSystem;
    
    nMC = chemsys.nMC;
    nC  = chemsys.nC;
    nR  = chemsys.nR;
    nS  = chemsys.nS;
    nCS = nC + nS;

    RM  = chemsys.reactionMatrix;
    SRM = chemsys.solidReactionMatrix;
    aRM = horzcat(RM, SRM);

    CM  = chemsys.compositionMatrix;
    SCM = chemsys.solidContributionMatrix;
    aCM  = horzcat(CM, SCM);

    logK = chemsys.logReactionConstants;
    K    = chemsys.reactionConstants;

    presentElements = model.getProp(state, 'presentElements');
    impossibleSolSpec = zeros(ncell, nCS);
    absentElements = ~presentElements;
    for i = 1 : nMC
        impossibleSolSpec = impossibleSolSpec + bsxfun(@times, aCM(i, :), ...
                                                       absentElements(:, i));
    end
    impossibleSolSpec = (impossibleSolSpec > 0);
    
    if doReviveSpecies
        % We assume that all the solids are present, meaning that absent solids
        % will not prevent an reaction equation of being active.
        presentSolids   = true(ncell, nS);
    end

    presentSpeSol = horzcat(presentSpecies, presentSolids);

    nonpresentSpecies = ~presentSpecies;
    nonpresentSolids  = ~presentSolids;
    nonpresentSpeSol  = ~presentSpeSol;
    
    [Lmults, Rmults, activeReactions] = setupRMmult(aRM, presentSpeSol);
    aRMstab = Rmults*aRM; 
    
    impossibleSolSpec = rldecode(impossibleSolSpec, nR, 1);
    impossibleReactions = abs(aRMstab).*impossibleSolSpec;
    impossibleReactions = (sum(impossibleReactions, 2) > 0);
    
    aRMstabPos = aRMstab;
    aRMstabPos(aRMstab < 0) = 0;
    aRMstabNeg = aRMstab;
    aRMstabNeg(aRMstab > 0) = 0;
    logKstab = vertcat(chemsys.logReactionConstants{:});
    logKstab = Rmults*logKstab;
    logKpos = -1/2*logKstab;
    logKneg = 1/2*logKstab;
    
    ncR = ncell*nR;
    isposaRM = zeros(ncR, nCS);
    isposaRM(aRMstab > 0) = 1;
    isnegaRM = zeros(ncR, nCS);
    isnegaRM(aRMstab < 0) = 1;

    posaRM = aRMstab;
    posaRM(aRMstab < 0) = 0;
    negaRM = aRMstab;
    negaRM(aRMstab > 0) = 0;
    negaRM = -negaRM; 
    % Note: coefficients of negaRM (and posaRM) are all positive
   
   
    % The solid is always assumed to be present and with activity one in the
    % reaction equation (hence, false vector below)
    np = horzcat(nonpresentSpecies, false(ncell, nS));
    np = rldecode(np, nR*ones(ncell, 1));
   
    nonwelldefLogPosP = sum(np.*isposaRM, 2);
    nonwelldefLogPosP = (nonwelldefLogPosP > 0);
    welldefLogPosP    = ~nonwelldefLogPosP;

    nonwelldefLogNegP = sum(np.*isnegaRM, 2);
    nonwelldefLogNegP = (nonwelldefLogNegP > 0);
    welldefLogNegP    = ~nonwelldefLogNegP;
    
    inactiveReactions = ~activeReactions;

    posP    = zeros(nR*ncell, 1);
    negP    = zeros(nR*ncell, 1);
    
    logActivities = [log(species), zeros(ncell, nS)];
    % Horrible hack
    logActivities(logActivities == -Inf) = 0;
    logActivities = rldecode(logActivities, nR*ones(ncell, 1));
    
    use = welldefLogPosP;
    poslogP = sum(posaRM(use, :).*logActivities(use, :), 2);
    posP(use) = exp(logKpos(use)).*exp(poslogP);
    
    use = welldefLogNegP;
    neglogP = sum(negaRM(use, :).*logActivities(use, :), 2);
    negP(use) = exp(logKneg(use)).*exp(neglogP);   
    
    rate = posP - negP;
    rate(activeReactions | impossibleReactions) = 0;

    % The kinetic equations contains a minus sign (our convention)
    % see paper.
    dx = -bsxfun(@times, aRMstab, rate);
    redop = sparse(rldecode((1 : ncell)', nR*ones(ncell, 1)), (1 : ncR)', 1, ...
                   ncell, ncR);
    dx = redop*dx;
    
    increasingSpeSol = false(ncell, nCS); 
    increasingSpeSol(dx > 0) = true;
    appearingSpeSol = ~presentSpeSol & increasingSpeSol;
    
    appearingSpecies = appearingSpeSol(:, [1 : nC]);
    species(appearingSpecies)        = exp(logspecval);
    logSpecies(appearingSpecies)     = logspecval;
    presentSpecies(appearingSpecies) = 1;

    state = model.setProp(state, 'species'        , species);
    state = model.setProp(state, 'logSpecies'     , logSpecies);
    state = model.setProp(state, 'presentSpecies' , presentSpecies);
    if doReviveSolids
        appearingSolids  = appearingSpeSol(:, [nC + 1 : nCS]);
        solidSpecies(appearingSolids)    = exp(logsolval);
        logSolidSpecies(appearingSolids) = logsolval;
        presentSolids(appearingSolids)   = 1;
        state = model.setProp(state, 'solidSpecies'   , solidSpecies);
        state = model.setProp(state, 'logSolidSpecies', logSolidSpecies);
        state = model.setProp(state, 'presentSolids'  , presentSolids);
    end
    
end


%{
Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
