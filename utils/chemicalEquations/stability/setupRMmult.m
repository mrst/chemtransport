function [Lmults, Rmults, ActiveReactions] = setupRMmult(RM, presentSpecies)

% ActiveReactions is set with respect to the new reaction equations
    
    [compositioncases, ~, presentSpeciesIndices] = unique(presentSpecies, ...
                                                      'rows');
    ccs = compositioncases;
    pis = presentSpeciesIndices;
    
    [ncell, nC] = size(presentSpecies);
    nR          = size(RM, 1);
    nccs        = size(ccs, 1);
    
    lmults = zeros(nccs*nR, nC);
    rmults = zeros(nccs*nR, nR);
    activeReactions = NaN(nccs*nR, 1);
    
    for i = 1 : nccs
        cc = ccs(i, :);
        
        M = RM(:, ~cc);
        [u, s, v] = svd(M);
        s = sum(s, 2);
        
        isnull = 10*eps;
        rankM = u(:, s > isnull);
        kerM  = u(:, s <= isnull);
        rmult = vertcat(kerM', rankM');

        activeReac = false(nR, 1);
        naR = nnz(s == 0);
        activeReac(1 : naR) = true;
        
        M = RM;
        M(:, cc) = 0;
        lmult = vertcat(kerM'*RM, rankM'*M);    
        
        lmults((i - 1)*nR + (1 : nR)', :) = lmult;
        rmults((i - 1)*nR + (1 : nR)', :) = rmult;
        activeReactions((i - 1)*nR + (1 : nR)') = activeReac;
    end
    
    ind1 = (1 : (nR*ncell))';
    ind2a = repmat((1 : nR)', ncell, 1);
    ind2b = rldecode(pis, nR*ones(ncell, 1));
    ind2b = ind2b - 1;
    ind2 = ind2a + ind2b*nR;
    dispatchM = sparse(ind1, ind2, 1, nR*ncell, nccs*nR);
    
    Lmults = dispatchM*lmults;
    Rmults = dispatchM*rmults;
    ActiveReactions = logical(dispatchM*activeReactions);
end



%{
Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
