function state = reviveSpeciesMassCons(model, state)
% We handle the special case where all constituting species of an elements
% are absent while the element is present. This is not handled below as we
% assume that mass conservation is fulfilled. If there is a present element
% for which none of the species that contain it are present, we set all the
% species as present with minimum values.
    chemsys = model.chemicalSystem;

    species         = model.getProp(state, 'species');
    logSpecies      = model.getProp(state, 'logSpecies');
    presentSpecies  = model.getProp(state, 'presentSpecies');
    solidSpecies    = model.getProp(state, 'solidSpecies');
    logSolidSpecies = model.getProp(state, 'logSolidSpecies');
    presentSolids   = model.getProp(state, 'presentSolids');
    presentElements = model.getProp(state, 'presentElements');

    logspecval = model.logSpeciesResetValue;
    logsolval  = model.logSolidsResetValue;

    CM  = chemsys.compositionMatrix;
    SCM = chemsys.solidContributionMatrix;
    aCM  = horzcat(CM, SCM);

    nMC = chemsys.nMC;
    nC  = chemsys.nC;
    nR  = chemsys.nR;
    nS  = chemsys.nS;

    nCS = nC + nS;

    ncell = size(species, 1);

    presentSpeSol = horzcat(presentSpecies, presentSolids);

    eltForwhichallspeciesAreMissing = presentSpeSol*aCM';
    eltForwhichallspeciesAreMissing = (eltForwhichallspeciesAreMissing == 0) & (presentElements);
    if any(any(eltForwhichallspeciesAreMissing))

        reviveSpecies = false(ncell, nC + nS);
        reviveSpecies(eltForwhichallspeciesAreMissing*aCM > 0) = true;

        reviveSolids  = reviveSpecies(:, ((nC + 1) : (nC + nS)));
        reviveSpecies = reviveSpecies(:, 1 : nC);

        species(reviveSpecies)        = exp(logspecval);
        logSpecies(reviveSpecies)     = logspecval;
        solidSpecies(reviveSolids)    = exp(logsolval);
        logSolidSpecies(reviveSolids) = logsolval;

        presentSpecies(reviveSpecies) = 1;
        presentSolids(reviveSolids)   = 1;

        species(reviveSpecies)        = exp(logspecval);
        logSpecies(reviveSpecies)     = logspecval;
        solidSpecies(reviveSolids)    = exp(logsolval);
        logSolidSpecies(reviveSolids) = logsolval;

        presentSpecies(reviveSpecies) = 1;
        presentSolids(reviveSolids)   = 1;

        state = model.setProp(state, 'species'        , species);
        state = model.setProp(state, 'logSpecies'     , logSpecies);
        state = model.setProp(state, 'presentSpecies' , presentSpecies);
        state = model.setProp(state, 'solidSpecies'   , solidSpecies);
        state = model.setProp(state, 'logSolidSpecies', logSolidSpecies);
        state = model.setProp(state, 'presentSolids'  , presentSolids);

    end
    
end



%{
Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
