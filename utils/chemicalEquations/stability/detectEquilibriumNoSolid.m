function state = detectEquilibriumNoSolid(model, state)
% The variable model is a chemical model 

    chemsys = model.chemicalSystem;

    species         = model.getProp(state, 'species');
    logSpecies      = model.getProp(state, 'logSpecies');
    presentSpecies  = model.getProp(state, 'presentSpecies');

    presentElements = model.getProp(state, 'presentElements');

    logspecval = model.logSpeciesResetValue;

    CM  = chemsys.compositionMatrix;
    RM  = chemsys.reactionMatrix;

    nMC = chemsys.nMC;
    nC  = chemsys.nC;
    nR  = chemsys.nR;

    ncell = size(species, 1);


    % We now handle the special case where all constituting species of an elements
    % are absent while the element is present. This is not handled below as we
    % assume that mass conservation is fulfilled. If there is a present element
    % for which none of the species that contain it are present, we set all the
    % species as present with minimum values.
    eltForwhichallspeciesAreMissing = presentSpecies*CM';
    eltForwhichallspeciesAreMissing = (eltForwhichallspeciesAreMissing == 0) & (presentElements);
    if any(any(eltForwhichallspeciesAreMissing))

        reviveSpecies = false(ncell, nC);
        reviveSpecies(eltForwhichallspeciesAreMissing*CM > 0) = true;

        species(reviveSpecies)        = exp(logspecval);
        logSpecies(reviveSpecies)     = logspecval;
        presentSpecies(reviveSpecies) = 1;

    end

    logK = chemsys.logReactionConstants;
    K    = chemsys.reactionConstants;

    nonpresentSpecies  = ~presentSpecies;

    [Lmults, Rmults, activeReactions] = setupRMmult(RM, presentSpecies);
    RMstab = Rmults*RM; 
    RMstabPos = RMstab;
    RMstabPos(RMstab < 0) = 0;
    RMstabNeg = RMstab;
    RMstabNeg(RMstab > 0) = 0;

    logKstab = vertcat(chemsys.logReactionConstants{:});
    logKstab = Rmults*logKstab;
    logKpos = 1/2*logKstab;
    logKneg = -1/2*logKstab;
    
    ncR = ncell*nR;
    isposRM = zeros(ncR, nC);
    isposRM(RMstab > 0) = 1;
    isnegRM = zeros(ncR, nC);
    isnegRM(RMstab < 0) = 1;

    posRM = RMstab;
    posRM(RMstab < 0) = 0;
    negRM = RMstab;
    negRM(RMstab > 0) = 0;
    
    np = rldecode(nonpresentSpecies, nR*ones(ncell, 1));
    nonwelldefLogPosP = sum(np.*isposRM, 2);
    nonwelldefLogPosP = (nonwelldefLogPosP > 0);
    welldefLogPosP    = ~nonwelldefLogPosP;

    nonwelldefLogNegP = sum(np.*isnegRM, 2);
    nonwelldefLogNegP = (nonwelldefLogNegP > 0);
    welldefLogNegP    = ~nonwelldefLogNegP;
    
    inactiveReactions = ~activeReactions;

    posP = zeros(nR*ncell, 1);
    negP = zeros(nR*ncell, 1);
    
    logActivities = log(species);
    % Horrible hack
    logActivities(logActivities == -Inf) = 0;
    logActivities = rldecode(logActivities, nR*ones(ncell, 1));
    
    use = welldefLogPosP;
    poslogP = sum(posRM(use, :).*logActivities(use, :), 2);
    posP(use) = exp(logKpos(use)).*exp(poslogP);
    
    use = welldefLogNegP;
    neglogP = sum(negRM(use, :).*logActivities(use, :), 2);
    negP(use) = exp(logKneg(use)).*exp(neglogP);   
    
    rate = posP - negP;
    rate(activeReactions) = 0;

    % The kinetic equations contains a minus sign (our convention)
    % see paper.
    dx = -bsxfun(@times, RMstab, rate);
    redop = sparse(rldecode((1 : ncell)', nR*ones(ncell, 1)), (1 : ncR)', 1, ...
                   ncell, ncR);
    dx = redop*dx;
    
    increasingSpecies = false(ncell, nC); 
    increasingSpecies(dx > 0) = true;
    appearingSpecies = ~presentSpecies & increasingSpecies;
    
    species(appearingSpecies)        = exp(logspecval);
    logSpecies(appearingSpecies)     = logspecval;
    presentSpecies(appearingSpecies) = 1;

    state = model.setProp(state, 'species'       , species);
    state = model.setProp(state, 'logSpecies'    , logSpecies);
    state = model.setProp(state, 'presentSpecies', presentSpecies);

    state = model.syncPrimaryFromState(state);
end



%{
Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
