function [eqs, names, types] = equationsInput(model, stateAD)

    inputValues = model.inputValues;
    inputNames  = model.inputNames;

    nEqs = numel(inputNames);
    
    eqs   = cell(1, nEqs);
    names = cell(1, nEqs);
    types = cell(1, nEqs);
    
    for i = 1 : nEqs
        stateADValue = model.getADProp(stateAD, inputNames{i});
        eqs{i} = stateADValue - inputValues(:, i);
        names{i} = sprintf('input %s', inputNames{i});
    end

    [types{:}] = deal('cell');
    
end




%{
Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
