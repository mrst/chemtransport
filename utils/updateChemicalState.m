function  state = updateChemicalState(model, primaryVars, state, state0)
% Truncate the solution using physically-based bounds.
% state0 is not needed if no surface chemistry
    
    chemsys = model.chemicalSystem;
    
    if chemsys.nP > 0
        surfParam = sum(cellfun(@(x) ~isempty(x) , regexpi(chemsys.surfaceActivityCoefficientNames, 'psi'))); 
        if surfParam > 0
            [names, mins, maxs] = computeMaxPotential(chemsys, state0); 
        end
    end

    nonLogVariables = removeLogFromNames(primaryVars); 

    len = cellfun(@(x) length(x), nonLogVariables);
    [~,sortInd] = sort(len(:),1, 'ascend');
    pVar = nonLogVariables(sortInd);

    LC = chemsys.combinationMatrix;
    CM = chemsys.compositionMatrix;


    for i = 1 : numel(pVar)

        p = pVar{i};
        compInd = strcmpi(p, chemsys.speciesNames);

        if any(strcmpi(p, chemsys.elementNames))
            state = model.capProperty(state, p, 1e-100, 300*mol/litre);
            
        elseif ~isempty(regexpi(p, 'psi'))
            ind = find(strcmpi(p, names));
            state = model.capProperty(state, p, mins{ind}, maxs{ind});
            
        elseif any(strcmpi(p, chemsys.combinationNames))
            ind = strcmpi(p, chemsys.combinationNames);
            
            conMat = CM;
            conMat(:,LC(ind,:)==0) = 0; 
            conMat = sum(conMat,2);
            maxvals = state.elements*conMat;
            state = model.capProperty(state, p, -maxvals, maxvals);
            
        elseif any(compInd)
            maxvals = chemsys.maxMatrices{compInd}*((state.elements)');
            maxvals = (min(maxvals))';
            state = model.capProperty(state, p, realmin, maxvals);

        end
        
        compInd = strcmpi(p, chemsys.solidNames);
        if any(compInd)
            maxvals = chemsys.maxSolidMatrices{compInd}*((state.elements)');
            maxvals = (min(maxvals))';
            state = model.capProperty(state, p, realmin, maxvals);
        end

    end

    state = model.syncLog(state);

end




%{
Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
