function  state = applyNaturalBounds(model, varsToBound, state, state0)
% Truncate the solution using physically-based bounds.
% state0 is not needed if no surface chemistry
%
% IMPORTANT : the variables are updated in the order given by varsToBound. This
% may influence the results. For the elements, a mechanism has been implemented
% so that they are updated first.
    
    chemsys = model.chemicalSystem;

    % Expand the names if necessary
    varsExpanded = {};
    for i = 1 : numel(varsToBound)
        var = varsToBound{i};
        switch var
          case 'elements'
            varsExpanded = horzcat(varsExpanded, chemsys.elementNames);
          case 'species'
            varsExpanded = horzcat(varsExpanded, chemsys.speciesNames);
          case 'solidSpecies'
            varsExpanded = horzcat(varsExpanded, chemsys.solidNames);
          otherwise
            varsExpanded{end + 1} = var;
        end
    end
    
    varsToBound = unique(varsExpanded, 'stable');
    
    % The elements must be updated first. This is important due to the
    % formulation of the natural bounds. Hence, we reorder varsToBound to get
    % the elements first.
    [lia, locb] = ismember(chemsys.elementNames, varsToBound);
    if any(lia)
        locb = locb(lia);
        n = numel(varsToBound);
        indothers = true(n, 1);
        indothers(locb) = false;
        indothers = find(indothers)';
        varsToBound = varsToBound([locb, indothers]);
    end
    
    LC = chemsys.combinationMatrix;
    CM = chemsys.compositionMatrix;

    for i = 1 : numel(varsToBound)

        var = varsToBound{i};
        compInd = strcmpi(var, chemsys.speciesNames);

        if any(strcmpi(var, chemsys.elementNames))
            % Elements are  capped to zero. The solver should be able to
            % handle that.
            state = model.capProperty(state, var, 0);
            if isprop(model, 'chopLogMaxValue')
                maxval = exp(model.chopLogMaxValue);
                state = model.capProperty(state, var, 0, maxval);
            end
            
        elseif ~isempty(regexpi(var, 'psi'))
            ind = find(strcmpi(var, names));
            state = model.capProperty(state, var, mins{ind}, maxs{ind});
            
        elseif any(strcmpi(var, chemsys.combinationNames))
            ind = strcmpi(var, chemsys.combinationNames);
            
            conMat = CM;
            conMat(:,LC(ind,:)==0) = 0; 
            conMat = sum(conMat,2);
            maxvals = state.elements*conMat;
            state = model.capProperty(state, var, -maxvals, maxvals);
            
        elseif any(compInd)
            maxvals = chemsys.maxMatrices{compInd}*((state.elements)');
            maxvals = (min(maxvals))';
            state = model.capProperty(state, var, 0, maxvals);

        end
        
        compInd = strcmpi(var, chemsys.solidNames);
        if any(compInd)
            maxvals = chemsys.maxSolidMatrices{compInd}*((state.elements)');
            maxvals = (min(maxvals))';
            state = model.capProperty(state, var, 0, maxvals);
        end
 
    end

    % Include synchronization of the log variables if there is a function for
    % that in the model
    if ismethod(model, 'syncLog')
        state = model.syncLog(state);
    end

end




%{
Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
