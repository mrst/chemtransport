classdef CalciteTransportLogModel < TransportNopressureLogModel


    methods

        function model = CalciteTransportLogModel(G, rock, fluid, bc, chemsys)
            model = model@TransportNopressureLogModel(G, rock, fluid, bc, chemsys);
        end
        
        function allvarnames = getAllVarNames(model)

            allvarnames = getAllVarNames@TransportNopressureLogModel(model);

            chemsys = model.chemicalSystem;            
            allvarnames = horzcat(allvarnames, {'solidSpecies'});
            allvarnames = horzcat(allvarnames, chemsys.solidNames);
        end

        function [eqs, names, types] = evaluateModelEquation(model, state0, stateAD, dt, drivingForces, varargin)
                    
            [eqs, names, types] = equationsCalciteTransport(state0, stateAD, ...
                                                              model, dt, ...
                                                              drivingForces, ...
                                                              varargin{:});
        end
        
        function [fn, index] = getVariableField(model, name, varargin)

            [fn, index] = getVariableField@ ...
                TransportNopressureLogModel(model, name);
            chemsys  = model.chemicalSystem;

            if ~isempty(index)
                return
            else
                varfound = false;
                failed   = false;
            end

            while ~varfound

                if strcmpi(name, 'solidSpecies')
                    varfound = true;
                    fn = 'solidSpecies';
                    index = ':';
                    break
                end

                ind = strcmpi(name, chemsys.solidNames);
                if any(ind)
                    varfound = true;
                    fn = 'solidSpecies';
                    index = find(ind);
                    break
                end        
                varfound = true; % to exit the loop
                failed   = true;
            end

            if failed
                [fn, index] = deal([]);
            end

        end

    end

end



%{
Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
