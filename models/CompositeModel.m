classdef CompositeModel < SimpleModel

    properties
        SubModels;
        SubModelNames;
        nSubModels;
        
        isCompositeModel;
    end

    methods
        function model = CompositeModel(varargin)
        % The constructor function should be complemented so that the properties
        % SubModels, SubModelNames are defined and the function
        % setupCompositeModel is called.
            model = model@SimpleModel(varargin{:});
        end
        
        function ind = getSubModelInd(model, name)
            ind = strcmp(name, model.SubModelNames);
        end
        
        function submodel = getSubModel(model, name)
            ind = model.getSubModelInd(name);
            submodel = model.SubModels{ind};
        end

        function model = setSubModel(model, name, submodel)
            ind = model.getSubModelInd(name);
            model.SubModels{ind} = submodel;
        end

        function model = setupCompositeModel(model)
            nsubmodels = numel(model.SubModels);
            model.nSubModels     = nsubmodels;
            model.isCompositeModel = true;
        end

        function [allvarnames, modelindex] = getAllVarNames(model)
        % default implementation should be model specific
            nsm = model.nSubModels;
            allvarnames = {};
            for i = 1 : nsm
                varnames = model.SubModels{i}.getAllVarNames();
                allvarnames = horzcat(allvarnames, varnames);
            end
            allvarnames = unique(allvarnames);
            navns = numel(allvarnames);
            modelindex = false(navns, nsm);
            for i = 1 : nsm
                varnames = model.SubModels{i}.getAllVarNames();
                [C, ia, ib] = intersect(varnames, allvarnames);
                modelindex(ib, i) = true;
            end
        end

        
        function model = setPrimaryVarNames(model)
            nsubmodels = model.nSubModels;
            primaryVarNames = {};
            for i = 1 : nsubmodels
                submodel          = model.SubModels{i};
                submodel          = submodel.setPrimaryVarNames();
                model.SubModels{i} = submodel;

                submodelprimaryvarnames = submodel.getPrimaryVarNames();
                primaryVarNames         = horzcat(primaryVarNames, ...
                                                  submodelprimaryvarnames);
            end
            model.primaryVarNames = primaryVarNames;
        end
        
        function [fn, index] = getVariableField(model, name, varargin)
            nsubmodels = model.nSubModels;

            found = false;
            i = 1;
            while ~found & (i <= nsubmodels)
                submodel = model.SubModels{i};
                [fn, index] = submodel.getVariableField(name);
                if ~isempty(index)
                    found = true;
                end
                i = i + 1;
            end
            assert(found, 'unknown variable');
        end

        function [state, report] = updateState(model, state, problem, dx, ...
                                               drivingForces)

            % Due to current use of function updateState, we need to
            % reinitiate the primary variable (this is unfortunate).
            model = model.setPrimaryVarNames();
            nsubmodels = model.nSubModels;
            for i = 1 : nsubmodels
                submodel   = model.SubModels{i};
                [state, ~] = submodel.updateState(state, problem, dx, []);
            end
            report = [];
        end

        function state = syncStateFromPrimary(model, state)
        % Not clear yet how we should use that. Problem when some variables
        % are shared between models.
            nsubmodels = model.nSubModels;
            for i = 1 : nsubmodels
                submodel = model.SubModels{i};
                state    = submodel.syncStateFromPrimary(state);
            end
        end

        function stateAD = syncStateADFromPrimary(model, stateAD)
        % Dangerous to use at the moment! Should be overwritten for each
        % instance of CompositeModel.
            nsubmodels = model.nSubModels;
            for i = 1 : nsubmodels
                submodel = model.SubModels{i};
                stateAD  = submodel.syncStateADFromPrimary(stateAD);
            end
        end

        function state = syncPrimaryFromState(model, state)
        % Compared to syncStateFromPrimary, this function should be safe, as the primary
        % variables are uniquely defined.
            nsubmodels = model.nSubModels;
            for i = 1 : nsubmodels
                submodel = model.SubModels{i};
                state    = submodel.syncPrimaryFromState(state);
            end
        end
        
        
        function [state, report] = updateAfterConvergence(model, state0, state, dt, drivingForces)
            nsubmodels = model.nSubModels;
            for i = 1 : nsubmodels
                submodel = model.SubModels{i};
                [state, ~] = submodel.updateAfterConvergence(state0, state, ...
                                                             dt, ...
                                                             drivingForces);
            end
            report = [];
        end
        
        function [valid, str] = checkModel(model)
            nsubmodels = model.nSubModels;
            
            % Check each submodel and that the declared variables of the
            % complex models corresponds to the submodels
            [allvarnames, modelindex] = model.getAllVarNames();
            valid = true; str = [];
            for i = 1 : nsubmodels
                submodel = model.SubModels{i};
                subvalid = submodel.checkModel();
                if ~subvalid
                    valid = false;
                    str = sprintf('model %s is invalid\n', model.SubModelNames{i});
                end
                submodelvarnames = submodel.getAllVarNames();
                declarednames = {allvarnames{modelindex(:, i)}};
                if ~isempty(setdiff(submodelvarnames, declarednames)) | ...
                        ~isempty(setdiff(declarednames, submodelvarnames))
                    valid = false;
                    str = sprintf('declared variables does not match with model %s\n', model.SubModelNames{i});

                end
            end
            
            % Check that the primary variables are unique not robust enough at the moment as
            % names are not unique identifiers and also problem with cell
            % structure (a variable name can correspond to a cell or to the index of a
            % cell)
            model = model.setPrimaryVarNames();
            primaryVarNames = model.getPrimaryVarNames();
            if numel(unique(primaryVarNames)) < numel(primaryVarNames)
                valid = false;
                str = sprintf('primary variables are not set up correctly\n');
            end
            

        end
        
        
    end
    

end



%{
Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
