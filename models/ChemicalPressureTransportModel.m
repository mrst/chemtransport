classdef ChemicalPressureTransportModel < CompositeModel

    properties
        % At the first iteration, we proceed with an transport step, followed
        % by chemistry solve (default is false)
        doFirstStepExplicit
        
        % Number of iterations where the chemistry is solve exactly (default
        % is zero)
        exactChemSolveIterations
        
    end
    
    methods

        function model = ChemicalPressureTransportModel(chemicalmodel, ...
                                                        transportmodel, ...
                                                        pressuremodel)
            
            % Change the input variables of the chemical model to the elements
            chemsys = chemicalmodel.chemicalSystem;
            chemsys.inputNames = chemsys.elementNames;
            chemsys.inputNames     = chemsys.elementNames;
            chemicalmodel.chemicalSystem = chemsys;
            
            model.SubModels     = {chemicalmodel, transportmodel, pressuremodel};
            model.SubModelNames = {'chemicalModel', 'transportModel', 'pressureModel'};

            
            model.doFirstStepExplicit      = false;
            model.exactChemSolveIterations = 0;
            
            model = model.setupCompositeModel();
        end

        function state = syncStateFromPrimary(model, state)
            % We use the the sub model handle the synchronization
            chemmodel  = model.getSubModel('chemicalModel');
            state = chemmodel.syncStateFromPrimary(state);

            transmodel = model.getSubModel('transportModel');
            state      = transmodel.syncStateFromPrimary(state);

            pressuremodel = model.getSubModel('pressureModel');
            state         = pressuremodel.syncStateFromPrimary(state);
            
        end

        function stateAD = syncStateADFromPrimary(model, stateAD)
        % The AD version of syncStateFromPrimary
            
            % We use the chemical model handle the synchronization
            chemmodel  = model.getSubModel('chemicalModel');
            stateAD = chemmodel.syncStateADFromPrimary(stateAD);
            
            transmodel = model.getSubModel('transportModel');
            stateAD    = transmodel.syncStateADFromPrimary(stateAD);

            pressuremodel = model.getSubModel('pressureModel');
            stateAD       = pressuremodel.syncStateADFromPrimary(stateAD);

        end

        function state = syncPresentFlagsFromState(model, state)
            
            transmodel = model.getSubModel('transportModel');
            state = transmodel.syncPresentFlagsFromState(state);
            
            chemmodel = model.getSubModel('chemicalModel');
            state = chemmodel.syncPresentFlagsFromState(state);

            % no flag in the pressure model
            
        end
        
        
        function [problem, state] = getEquations(model, state0, state, dt, drivingForces, ...
                                                        varargin)

            opt = struct('Verbose', mrstVerbose, ...
                         'reverseMode', false,...
                         'resOnly', false,...
                         'iteration', -1);  % Compatibility only
            opt = merge_options(opt, varargin{:});


            %Initialization of primary variables
            if opt.resOnly
                stateAD = convertToCell(state);
            else
                [model, state, stateAD]  =  model.initADState(state);
            end

            chemmodel     = model.getSubModel('chemicalModel');
            transmodel    = model.getSubModel('transportModel');
            pressuremodel = model.getSubModel('pressureModel');
            
            [chem_eqs, chem_names, chem_types] = ...
                chemmodel.evaluateModelEquation(state0, stateAD, dt, []);


            state0 = convertToCell(state0);
            [tr_eqs, tr_names, tr_types] = transmodel.evaluateModelEquation(state0, ...
                                                              stateAD, dt, []);

            [p_eqs, p_names, p_types] = pressuremodel.evaluateModelEquation(state0, ...
                                                              stateAD, dt, []);
            
            eqs   = horzcat(p_eqs, tr_eqs, chem_eqs);
            names = {p_names{:}, tr_names{:}, chem_names{:}};
            types = {p_types{:}, tr_types{:}, chem_types{:}};
            pVars = model.getPrimaryVarNames();

            problem = LinearizedProblem(eqs, types, names, pVars, state, dt);


        end

                    
        function [state, report] = stepFunction(model, state, state0, dt, drivingForces, linsolver, nonlinsolver, iteration, varargin)

            
            if model.doFirstStepExplicit & (iteration == 1 )
                error('do not want to use that for now')
                % Update element concentation with an explicit transport step
                transmodel = model.getSubModel('transportModel');
                state = transmodel.stepFunction(state, state0, dt, drivingForces, ...
                                                linsolver, nonlinsolver, 1);
                varsToBound = {'elements'};
                state = applyNaturalBounds(transmodel, varsToBound, state);
                
                % Update chemical system using the new element concentration
                chemmodel = model.getSubModel('chemicalModel');
                
                % Sync the variables before call of solveChemicalState
                varnames    = {'elements'};
                logvarnames = {'logElements'};
                state = syncLogVariable(model, state, varnames, logvarnames);
                [state, failure, report] = chemmodel.solveChemicalState(state);
            end
            
            chemmodel = model.getSubModel('chemicalModel');
            state = detectEquilibriumNoSolid(chemmodel, state);
            % Proceed with an fully-implicit step
            [state, report] = stepFunction@PhysicalModel(model, state, state0, ...
                                                         dt, drivingForces, ...
                                                         linsolver, nonlinsolver, ...
                                                         iteration, varargin{:});

            if iteration < model.exactChemSolveIterations 
                error('do not want to use that for now')
                % Update chemical system using the new element concentration
                chemmodel = model.getSubModel('chemicalModel');
                [state, failure, ~] = chemmodel.solveChemicalState(state);
                % We do not handle the case where this step fails for now. It is thus safer to
                % throw an error.
                assert(~failure, 'internal chemical state computation did not converge')
            end
            
        end
        
        function state = applyPhysicalConstraints(model, state)
            transmodel = model.getSubModel('transportModel');
            chemmodel  = model.getSubModel('chemicalModel');
            state = transmodel.applyPhysicalConstraints(state);
            state = chemmodel.applyPhysicalConstraints(state);
        end
        
        
        function [state, report] = updateState(model, state, problem, dx, drivingForces) %#ok
        % Update state based on Newton increments
            
            model = model.setPrimaryVarNames(); % primary variables are lost due to
                                                % simulateSchedule mechanism
            
            [state, report] = updateState@PhysicalModel(model, state, problem, ...
                                                        dx, drivingForces);
        
            % update the other variables from the primary variables
            state = model.syncStateFromPrimary(state);
            state = model.applyPhysicalConstraints(state);
            state = model.capProperty(state, 'pressure', 0);
            state = model.syncPrimaryFromState(state);

        end

        function [state, report] = updateAfterConvergence(model, state0, state, ...
                                                          dt, drivingForces)
            [state, report] = updateAfterConvergence@CompositeModel(model, state0, ...
                                                              state, dt, ...
                                                              drivingForces);
            state = model.syncPresentFlagsFromState(state);

            if model.debugmode
                chemmodel = model.getSubModel('chemicalModel');
                debugPlot(chemmodel, state);
            end
        end
        
        function forces = getValidDrivingForces(model)
        % To avoid warning from simulateScheduleAD
            forces = struct('W'  , [], ...
                            'bc' , [], ...
                            'src', []);
        end

        function state = validateState(model, state)
            state.wellSol = [];
        end
    end
end



%{
Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
