classdef TransportModel < SimpleModel

    properties
        rock
        fluid
        chemicalSystem

        % Matrices of size (nMC, nC) with logical coefficients which are true if the
        % species in a given element is attached to the surface
        % (surfaceComponents) or flowing with the liquid (fluidComponents).
        surfaceComponents
        fluidComponents

        primvarElementsNames
        
        % boundary conditions: The bc structure contains the following fields
        % 'dirCell'     : Cell where pressure is imposed
        % 'dirPressure' : Value of the pressure        
        % 'influxcells' : Cells where injection takes place
        % 'influxrates' : Injection rate
        % 'influxConc'  : Concentration of the elements (NB: not species) of
        %                 the solution that is injected
        bc

        chopLogMaxValue % chopping value for log of elements
        chopLogMaxValueHO % chopping value for log of H and O
        
        % log-value for detection of element disappearance, see function
        % syncPresentSolidsAndSatindsFromState
        logElementsDisappearenceValue;
        logElementsResetValue;

    end

    methods

        function model = TransportModel(G, rock, fluid, bc, chemsys)
            model      = model@SimpleModel();

            model.G     = G;
            model.rock  = rock;
            model.fluid = fluid;
            model.bc    = bc;
            model.chemicalSystem = chemsys;

            model = model.setupFluidComponents();

            elementNames = chemsys.elementNames;
            nMC = chemsys.nMC;
            primvarElementsNames = cell(1, nMC);
            for i = 1 : nMC
                primvarElementsNames{i} = strcat(elementNames{i}, '_pvar');
            end
            model.primvarElementsNames = primvarElementsNames;

            model.operators = setupOperatorsTPFA(G, rock);

            model.logElementsDisappearenceValue = -30;
            model.logElementsResetValue         = -40;

            model.chopLogMaxValueHO = 10;
            model.chopLogMaxValue = 10;
            
        end

        function model = setupFluidComponents(model)
            chemsys = model.chemicalSystem;

            nC  = chemsys.nC;
            nMC = chemsys.nMC;
            CM  = chemsys.compositionMatrix;

            surfMaster  = logical(chemsys.surfMaster);
            surfComp    = sum(logical(CM(surfMaster, :)), 1);
            surfMult    = repmat(surfComp, nMC, 1);
            surfMatFlag = logical(CM.*surfMult);

            surfMat                 = zeros(nMC, nC);
            surfMat(surfMatFlag)    = CM(surfMatFlag);
            model.surfaceComponents = surfMat;

            fluidMat               = zeros(nMC, nC);
            fluidMat(~surfMatFlag) = CM(~surfMatFlag);
            model.fluidComponents  = fluidMat;
        end

        function allvarnames = getAllVarNames(model)
            chemsys = model.chemicalSystem;

            allvarnames = {'species', 'elements', 'logElements', 'presentElements', ...
                           'primvarElements'};
            allvarnames = horzcat(allvarnames, chemsys.speciesNames);
            allvarnames = horzcat(allvarnames, chemsys.elementNames);
            allvarnames = horzcat(allvarnames, chemsys.logElementNames);
            allvarnames = horzcat(allvarnames, model.primvarElementsNames);
            allvarnames = horzcat(allvarnames, {'pressure', 'p'});
        end

        function model = setPrimaryVarNames(model)
            primaryVarNames = model.primvarElementsNames;
            model.primaryVarNames = {primaryVarNames{:}, 'pressure'};
        end

        function state = syncPrimaryFromState(model, state)
            chemsys = model.chemicalSystem;
            nMC     = chemsys.nMC;

            presentElements = model.getProp(state, 'presentElements');
            elements        = model.getProp(state, 'elements');

            primvarElements = elements;
            for i = 1 : nMC
                presentElement = presentElements(:,  i);
                primvarElements(~presentElement, i) = 0;
            end

            state = model.setProp(state, 'primvarElements', primvarElements);

        end

        function state = syncStateFromPrimary(model, state)
            chemsys = model.chemicalSystem;
            nMC      = chemsys.nMC;

            presentElements = model.getProp(state, 'presentElements');
            primvarElements = model.getProp(state, 'primvarElements');

            elements    = primvarElements;
            logElements = elements; % dummy values, get correct dimensions

            for i = 1 : nMC
                presentElement = presentElements(:,  i);
                logElements(presentElement, i)  = log(elements(presentElement, ...
                                                              i));
                logElements(~presentElement, i) = NaN;
            end

            state = model.setProp(state, 'elements', elements);
            state = model.setProp(state, 'logElements', logElements);

        end

        function stateAD = syncStateADFromPrimary(model, stateAD)
            chemsys = model.chemicalSystem;
            nMC      = chemsys.nMC;

            presentElements = model.getADProp(stateAD, 'presentElements');
            primvarElements = model.getADProp(stateAD, 'primvarElements');

            for i = 1 : nMC
                presentElement = presentElements{i};

                elements{i}    = primvarElements{i};
                logElements{i} = elements{i}; % dummy values, get correct dimensions
                logElements{i}(presentElement)  = log(elements{i}(presentElement));
                logElements{i}(~presentElement) = NaN;
            end

            stateAD = model.setADProp(stateAD, 'elements', elements);
            stateAD = model.setADProp(stateAD, 'logElements', logElements);
        end


        function state = syncPresentFlagsFromState(model, state)
            state = model.syncPresentElementsFromState(state);
        end


        function state = syncPresentElementsFromState(model, state)

            elements    = model.getProp(state, 'Elements');
            logElements = model.getProp(state, 'logElements');

            ldisval = model.logElementsDisappearenceValue;
            presentElements = (elements > exp(ldisval));
            elements(~presentElements)    = 0;
            logElements(presentElements)  = log(elements(presentElements));
            logElements(~presentElements) = NaN;

            state = model.setProp(state, 'presentElements', presentElements);
            state = model.setProp(state, 'logElements'    , logElements);
            state = model.setProp(state, 'elements'       , elements);

        end

        function [problem, state] = getEquations(model, state0, state, dt, drivingForces, ...
                                                        varargin)
            opt = struct('Verbose', mrstVerbose, ...
                         'reverseMode', false,...
                         'resOnly', false,...
                         'iteration', -1);  % Compatibility only
            opt = merge_options(opt, varargin{:});
            
            % Initialization of primary variables
            if opt.resOnly
                stateAD = convertToCell(state);
            else
                [model, state, stateAD]  =  model.initADState(state);
            end

            state0 = convertToCell(state0);
            [eqs, names, types] = model.evaluateModelEquation(state0, stateAD, ...
                                                              dt, []);
            pVars = model.getPrimaryVarNames();
            problem = LinearizedProblem(eqs, types, names, pVars, state, dt);

        end

        function [eqs, names, types] = evaluateModelEquation(model, state0, stateAD, dt, drivingForces, varargin)
            
            [eqs, names, types] = equationsTransport(state0, stateAD, model, ...
                                                     dt, drivingForces, ...
                                                     varargin{:});
        end
        
        function state = applyPhysicalConstraints(model, state)
            chemsys = model.chemicalSystem;
            elementNames = chemsys.elementNames;
            maxval   = exp(model.chopLogMaxValue);
            maxvalHO = exp(model.chopLogMaxValueHO);
            for i = 1 : numel(elementNames)
                elementName = elementNames{i};
                element = model.getProp(state, elementName);
                if ismember(elementName, {'H', 'O'})
                    element = min(element, maxvalHO);
                else
                    element = min(element, maxval);                    
                end
                element = max(element, 0);
                state = model.setProp(state, elementName, element);
            end
            state = model.syncPresentElementsFromState(state);
        end
        
        function [state, report] = updateState(model, state, problem, dx, ...
                                               drivingForces) %#ok
            [state, report] = updateState@PhysicalModel(model, state, problem, ...
                                                        dx, drivingForces);
            error('not updated')
            state = model.syncPresentElementsFromState(state);

        end

        function [fn, index] = getVariableField(model, name, varargin)

            chemsys  = model.chemicalSystem;
            varfound = false;
            failed   = false;

            while ~varfound

                if strcmpi(name, 'species')
                    varfound = true;
                    fn = 'species';
                    index = ':';
                    break
                end

                if strcmpi(name, 'elements')
                    varfound = true;
                    fn = 'elements';
                    index = ':';
                    break
                end

                if strcmpi(name, 'logElements')
                    varfound = true;
                    fn = 'logElements';
                    index = ':';
                    break
                end

                if strcmpi(name, 'presentElements')
                    varfound = true;
                    fn = 'presentElements';
                    index = ':';
                    break
                end

                if strcmpi(name, 'primvarElements')
                    varfound = true;
                    fn = 'primvarElements';
                    index = ':';
                    break
                end

                ind = strcmpi(name, model.primvarElementsNames);
                if any(ind)
                    varfound = true;
                    fn = 'primvarElements';
                    index = find(ind);
                    break
                end

                ind = strcmpi(name, chemsys.speciesNames);
                if any(ind)
                    varfound = true;
                    fn = 'species';
                    index = find(ind);
                    break
                end

                ind = strcmpi(name, chemsys.elementNames);
                if any(ind)
                    varfound = true;
                    fn = 'elements';
                    index = find(ind);
                    break
                end

                ind = strcmpi(name, chemsys.logElementNames);
                if any(ind)
                    varfound = true;
                    fn = 'logElements';
                    index = find(ind);
                    break
                end

                if any(strcmpi(name, {'p', 'pressure'}))
                    fn    = 'pressure';
                    index = 1;
                    break;
                end

                varfound = true; % to exit the loop
                failed   = true;

            end

            if failed
                [fn, index] = deal([]);
            end

        end

    end
end



%{
Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
