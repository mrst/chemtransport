classdef SimpleModel < PhysicalModel

    properties
        % list of the primary variables that will be used for this model.
        % "Exact" names (as defined uniquely by getVariableField) should be
        % used when setting up the primary variable names.
        primaryVarNames;
        
        debugmode;
    end

    methods
        function model = SimpleModel(varargin)
            model = model@PhysicalModel([]);
            model.debugmode = false;
            model = merge_options(model, varargin{:});
        end

        function varnames = getPrimaryVarNames(model)
        % list all the primarty variables that are recognized and can be
        % handled by the model, used by the updateState member function.
            varnames = model.primaryVarNames;
        end

        function model = setPrimaryVarNames(model)
        % set the primarty variables, should be implemented for each
        % intentiation.
        % OBS : for the moment, the name should be the unique name as defined
        % by getVariableField (because of mechanisms used in updateState)
            error('not meant to be used for the base class');
        end

        function allvarnames = getAllVarNames(model)
        % model specific
        end

        function pAD = getADProp(model, stateAD, name)
            [fn, index] = model.getVariableField(name);
            if ~iscell(stateAD.(fn))
                % The function should also be able to handle array case
                pAD = stateAD.(fn)(:, index);
            else
                if isnumeric(index)
                    pAD = stateAD.(fn){index};
                else
                    pAD = stateAD.(fn);
                end
            end
        end

        function stateAD = setADProp(model, stateAD, name, value)
            [fn, index] = model.getVariableField(name);
            % % following lines have been commented but not yet tested.
            % if ~iscell(stateAD.(fn))
            %     % The function should also be able to handle array case
            %     stateAD.(fn)(:, index) = value;
            % else
            if isnumeric(index)
                stateAD.(fn){index} = value;
            else
                stateAD.(fn) = value;
            end
        end

        function [model, state, stateAD] = initADState(model, state)
            % First setup the primary variable names for this step 
            model = model.setPrimaryVarNames();
            % Synchronize primary variables as prescribed by the model
            state = model.syncPrimaryFromState(state);
            
            % Set the primary variables as AD.
            primaryVars = model.getPrimaryVarNames();
            nvars = numel(primaryVars);
            vars = cell(nvars, 1);
            [vars{:}] = model.getProps(state, primaryVars{:});
            [vars{:}] = initVariablesADI(vars{:});
            stateAD = convertToCell(state);
            for i = 1 : nvars
                stateAD = model.setADProp(stateAD, primaryVars{i}, vars{i});
            end
            stateAD = model.syncStateADFromPrimary(stateAD);
        end

        function [state, report] = updateState(model, state, problem, dx, ...
                                               drivingForces)
            % updates the state variable that belongs to the model
            % Due to current use of function updateState, we need to
            % reinitiate the primary variable (this is unfortunate).
            if isprop(model, 'isCompositeModel')
                primaryvarnames = model.getPrimaryVarNames();
            else
                model           = model.setPrimaryVarNames();
                primaryvarnames = model.getPrimaryVarNames();
            end
            
            for i = 1:numel(primaryvarnames)
                p = primaryvarnames{i};
                % Update the state
                state = model.updateStateFromIncrement(state, dx, problem, p);
            end
            report = [];

        end

        
        function state = syncStateFromPrimary(model, state)
        % Use values of the primary variables to synchronize the other
        % variables in the system 
            return;
        end

        function stateAD = syncStateADFromPrimary(model, stateAD)
        % Use values of the primary variables to synchronize the other
        % variables (AD compatible version)
            return;
        end

        function stateAD = syncPrimaryFromState(model, stateAD)
        % Use values of the state variables to synchronize the primary
        % variables.
        % This is useful when the primary variables are helper variables
        % (see AqueousReactionModel and
        % equationsAqueousReaction for example)
            return;
        end
        
        function valid = checkModel(model)
            allvarnames = model.getAllVarNames();
            valid = true;
            for i = 1 : numel(allvarnames)
                [~, index] = getVariableField(model, allvarnames{i});
                if isempty(index)
                    valid = false;
                end
            end
        end

    end

end



%{
Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
