classdef AqueousReactionInputChargeBalanceModel < AqueousReactionInputModel


    
    methods

        function model = AqueousReactionInputChargeBalanceModel(chemmodel, inputmodel)
            model = model@AqueousReactionInputModel(chemmodel, inputmodel)
        end

        
        function [problem, state] = getEquations(model, state0, state, dt, drivingForces, ...
                                                        varargin)

            opt = struct('Verbose'    , mrstVerbose, ...
                         'reverseMode', false      ,...
                         'resOnly'    , false      ,...
                         'iteration'  , -1);  % Compatibility only
            opt = merge_options(opt, varargin{:});


            % Initialization of primary variables
            if opt.resOnly
                stateAD = convertToCell(state);
            else
                [model, state, stateAD]  =  model.initADState(state);
            end

            chemmodel  = model.getSubModel('chemicalModel');
            inputmodel = model.getSubModel('inputModel');

            [chem_eqs, chem_names, chem_types] = equationsAqueousReaction(chemmodel, ...
                                                              stateAD);
            
            [charge_eqs, charge_names, charge_types] = equationsAqueousReaction(chemmodel, ...
                                                              stateAD);

            [input_eqs, input_names, input_types] = equationsInput(inputmodel, ...
                                                              stateAD);
            eqs   = horzcat(input_eqs, chem_eqs, charge_eqs);
            names = {input_names{:}, chem_names{:}, charge_names{:}};
            types = {input_types{:}, chem_types{:}, charge_types{:}};
            pVars = model.getPrimaryVarNames();

            problem = LinearizedProblem(eqs, types, names, pVars, state, dt);

        end
        
        function state = setInputValues(model, state)
            inputmodel = model.getSubModel('inputModel');
            inputNames = inputmodel.inputNames;
            inputValues = inputmodel.inputValues;
            for i = 1 : numel(inputNames)
                state = model.setProp(state, inputNames{i}, inputValues(:, i));
            end
            state = syncPresentFlagsFromState(model, state);
        end
        
    end
end



%{
Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
