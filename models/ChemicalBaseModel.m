classdef ChemicalBaseModel < SimpleModel
% model for initializing and solving the chemical system based on user chosen input
    
%{
Copyright 2009-2016 SINTEF DIGITAL, Applied Mathematics and Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
    
    properties
        chemicalSystem;
        plotIterations         % toggle plotting of iteration information true or false
        nonLinearMinIterations % minimum number of iterations for the nonlinear solver
        nonLinearMaxIterations % maximum number of iterations for the nonlinear solver
        linearTolerance        % tolerance of the residual of the linear system, for backslash
        linearMaxIterations    % maximum number of iterations for the linear solver
    end
    
    methods
        
        function model = ChemicalBaseModel(chemsys, varargin)
            model = model@SimpleModel();
            model.chemicalSystem = chemsys;

            model.plotIterations         = false;
            model.nonLinearMinIterations = 1;
            model.nonLinearMaxIterations = 25;
            model.linearMaxIterations    = 25;
            model.linearTolerance        = 1e-8;
            model.linearTolerance        = 1e-8;
        end
        
        function model = setPrimaryVarNames(model)
            chemsys = model.chemicalSystem;
            primaryVarNames = horzcat(chemsys.speciesNames, ...
                                      chemsys.elementNames, ...
                                      chemsys.solidNames);
            
            ind = ismember(primaryVarNames, chemsys.inputNames);
            primaryVarNames = primaryVarNames(~ind);
            model.primaryVarNames = addLogToNames(primaryVarNames);
        end

        
        function [problem, state] = getEquations(model, state0, state, dt, drivingForces, varargin)
            error('This is a base Model, not meant to be used directly')
        end
        
        function [state, failure, report] = solveChemicalState(model, inputstate)
        % inputstate contains the input and the initial guess.  
            
        % NOTE : The variables in inpustate (in particular the log variables) must be in
        % sync.

            solver = NonLinearSolver();

            solver.maxIterations              = model.nonLinearMaxIterations;
            solver.minIterations              = model.nonLinearMinIterations;
            solver.LinearSolver.tolerance     = model.linearTolerance;
            solver.LinearSolver.maxIterations = model.linearMaxIterations;

            dt            = 0; % dummy timestep
            drivingForces = []; % drivingForces;
            inputstate0   = inputstate;

            [state, failure, report] = solveMinistep(solver, model, inputstate, ...
                                                     inputstate0, dt, ...
                                                     drivingForces);
            
            converged = report.Converged;
            if ~converged
                nlreport = report.NonlinearReport{end};
                failure  = nlreport.Failure;
                if failure
                    msg = ['Model step resulted in failure state. Reason: ', ...
                           nlreport.FailureMsg]; %#ok<AGROW>
                    error(msg);
                end
            end
            
        end

        function [state, report, model] = initState(model, userInput, varargin)
        % initState solves the chemical system using the specified input
                    
            chemsys = model.chemicalSystem;
            
            % parse inputs to initState

            p = inputParser;
            
            valFun = @(x) ischar(x);
            p.addParameter('chargeBalance', 'nochargebalance', valFun);
            p.addParameter('temperature'  , 298              , @isnumeric);

            
            p.parse(varargin{:})
            
            if strcmpi('nochargebalance', p.Results.chargeBalance)
                % this is ok
            elseif any(ismember({'e','e-'}, p.Results.chargeBalance))
                error('Electron concentration, e or e-, can not be used for charge balance.')
            elseif sum(ismember(chemsys.elementNames,p.Results.chargeBalance))==0
                warning('Using any quantitiy other than a total element concentration as a charge balance can yield unexpected results.')
            end
            
            nI = size(userInput,1);
            
            % grab the temperature
            nTemp = size(p.Results.temperature, 1);
            if nTemp == 1
                state.temperature = repmat(p.Results.temperature, nI, 1);
            else
                assert(nTemp == nI, 'The number of cells in state.temperature do not correspond to the size of userInput. Repmat the input vector if you want to do a temperature sweep.');
                state.temperature = p.Results.temperature(:);
            end

            
            givenTest = any(strcmpi(p.Results.chargeBalance, ...
                                    horzcat(chemsys.inputNames, ...
                                            'nochargebalance')));
            if ~givenTest
                warning 'Using a total element concentration whos values are not given (marked with "*") for charge balance may result in unexpected results.';
            end
            
            chargeBalance = ~strcmpi(p.Results.chargeBalance, 'nochargebalance');

            % compositionReactionModel = CompositionReactionModel();
            % props = properties(ChemicalInputModel);
            % for i = 1 : numel(props)
            %     if ~strcmp(props{i}, 'chemicalInputModel')
            %         compositionReactionModel.(props{i}) = cheminput.(props{i});
            %     end
            % end
            % compositionReactionModel = compositionReactionModel.validateModel();


            inSize = size(userInput);
            if ~isempty(chemsys.surfInfo);
                k = numel(chemsys.surfInfo.master);
            else
                k = 0;
            end
            
            assert(inSize(2) == chemsys.nMC-k, ['For the specified chemical system the input constraint must have ' ...
                                num2str(chemsys.nMC-k) ' columns.']);
            
            % Initiate state 
            state.elements = mol/litre*ones(size(userInput,1), chemsys.nMC);
            state.species  = mol/litre*ones(size(userInput,1), chemsys.nC);
            
            inputNames = chemsys.inputNames;
            
            % make sure input is the correct size
            givenSize = cellfun(@(x) size(x, 1), in);
            mSize     = max(givenSize);
            assert(any(givenSize == mSize | givenSize == 1), 'Input size must be consistent between input and surface parameters');
            
            % set their values inside of state
            for i = 1 : chemsys.nMC
                state = model.setProp(state, inputNames{i}, in{i}.*ones(mSize, ...
                                                                  1));
            end
            % Sync the log variables
            state = syncLog(model, state);
            
            % solve chemical system
            if chargeBalance
                fprintf('Solving the chemical system with strict charge balance...\n');
                state0 = state;
                chargeBalanceModel = ChargeBalanceModel(chemsys);
                chargeBalanceModel = chargeBalanceModel.setCVC(p.Results.chargeBalance);
                [state, ~, report] = chargeBalanceModel.solveChemicalState(state);
                if ~report.Converged
                    state = state0;
                    warning('Charge balance not achieved with given inputs, consider choosing a different component for charge compensation. Attempting solution without charge balance.');
                end
            else
                fprintf('Solving chemical system...\n')
                [state, ~, report] = model.solveChemicalState(state);
                if ~report.Converged
                    warning('Solver did not converge, use results with caution.');
                end
            end
            
        end

        function printChemicalSystem(model, varargin)
        % printChemicalSystem prints the mass conservation, law of mass
        % action, and linear combination equations to the workspace
            chemicalSystemPrintFunction(model.chemicalSystem, varargin{:});
        end

        function state = syncLog(model, state)
        % Sync the log variables
            varnames    = {'species', 'elements'};
            logvarnames = {'logSpecies', 'logElements'};
            state = syncLogVariable(model, state, varnames, logvarnames);
        end

        function state = syncFromLog(model, state)
        % Sync the log variables
            varnames    = {'species', 'elements'};
            logvarnames = {'logSpecies', 'logElements'};
            state = syncFromLogVariable(model, state, varnames, logvarnames);
        end
        
        function state = syncStateFromPrimary(model, state)
        % Use values of the primary variables to synchronize the other
        % variables in the system 
            state = model.syncFromLog(state);
        end

        function [fn, index] = getVariableField(model, name, varargin)

            chemsys  = model.chemicalSystem;
            varfound = false;
            failed   = false;

            while ~varfound


                if strcmpi(name, 'species')
                    varfound = true;
                    fn = 'species';
                    index = ':';
                    break
                end


                ind = strcmpi(name, chemsys.speciesNames);
                if any(ind)
                    varfound = true;
                    fn = 'species';
                    index = find(ind);
                    break
                end

                if strcmpi(name, 'logSpecies')
                    varfound = true;
                    fn = 'logSpecies';
                    index = ':';
                    break
                end

                ind = strcmpi(name, chemsys.logSpeciesNames);
                if any(ind)
                    varfound = true;
                    fn = 'logSpecies';
                    index = find(ind);
                    break
                end

                if strcmpi(name, 'elements')
                    varfound = true;
                    fn = 'elements';
                    index = ':';
                    break
                end


                ind = strcmpi(name, chemsys.elementNames);
                if any(ind)
                    varfound = true;
                    fn = 'elements';
                    index = find(ind);
                    break
                end

                if strcmpi(name, 'logElements')
                    varfound = true;
                    fn = 'logElements';
                    index = ':';
                    break
                end

                ind = strcmpi(name, chemsys.logElementNames);
                if any(ind)
                    varfound = true;
                    fn = 'logElements';
                    index = find(ind);
                    break
                end

                if strcmpi(name, 'solidSpecies')
                    varfound = true;
                    fn = 'solidSpecies';
                    index = ':';
                    break
                end
                
                ind = strcmpi(name, chemsys.solidNames);
                if any(ind)
                    varfound = true;
                    fn = 'solidSpecies';
                    index = find(ind);
                    break
                end

                if strcmpi(name, 'logSolidSpecies')
                    varfound = true;
                    fn = 'logSolidSpecies';
                    index = ':';
                    break
                end

                ind = strcmpi(name, chemsys.logSolidNames);
                if any(ind)
                    varfound = true;
                    fn = 'logSolidSpecies';
                    index = find(ind);
                    break
                end

                if strcmpi(name, 'presentSolids')
                    varfound = true;
                    fn = 'presentSolids';
                    index = ':';
                    break
                end

                ind = strcmpi(name, 'temperature');
                if any(ind)
                    varfound = true;
                    fn = 'temperature';
                    index = find(ind);
                    break
                end

                if strcmpi(name, 'chargeBalance')
                    varfound = true;
                    fn = 'chargeBalance';
                    index = ':';
                    break
                end
                
                varfound = true; % to exit the loop
                failed   = true;
            end

            if failed
                [fn, index] = deal([]);
            end

        end


    end

end



%{
Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
