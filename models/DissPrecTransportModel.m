classdef DissPrecTransportModel < ChemicalTransportBaseModel

    methods

        function model = DissPrecTransportModel(dissprecmodel, transportmodel)
            model = model@ChemicalTransportBaseModel(dissprecmodel, transportmodel);
        end


        function [state, report] = stepFunction(model, state, state0, dt, drivingForces, linsolver, nonlinsolver, iteration, varargin)

            chemmodel = model.getSubModel('chemicalModel');
            state = detectEquilibrium(chemmodel, state);
            % Proceed with an fully-implicit step
            [state, report] = stepFunction@PhysicalModel(model, state, state0, ...
                                                         dt, drivingForces, ...
                                                         linsolver, nonlinsolver, ...
                                                         iteration, varargin{:});
            if iteration < model.exactChemSolveIterations 
                % Update chemical system using the new element concentration
                chemmodel = model.getSubModel('chemicalModel');
                
                chemsys     = chemmodel.chemicalSystem;
                inputNames  = chemsys.elementNames;
                inputValues = chemmodel.getProps(state, 'elements');
                
                [state2, failure, chemreport] = ...
                    chemmodel.solveChemicalState(inputNames, inputValues, ...
                                                 state);
                if ~(chemreport.Converged)
                    warning(['internal chemical state computation did not ' ...
                             'converge']);
                else
                    state = state2;
                end
            end
        end
        

    end
end



%{
Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
