classdef InputLogModel < SimpleModel

    properties
        chemicalSystem

        primvarElementsNames

        chopLogMaxValue % chopping value for log of elements
        
        % log-value for detection of element disappearance, see function
        % syncPresentElementsFromState
        logElementsDisappearenceValue;
        logElementsResetValue; % not used for the moment

        inputValues
        inputNames

    end

    methods

        function model = InputLogModel(chemsys, inputNames, inputValues)

            model = model@SimpleModel();

            elementNames = chemsys.elementNames;
            nMC          = chemsys.nMC;
            primvarElementsNames = cell(1, nMC);
            for i = 1 : nMC
                primvarElementsNames{i}   = strcat(elementNames{i}, '_pvar');
            end
            model.primvarElementsNames = primvarElementsNames;

            model.chemicalSystem = chemsys;
            
            model.logElementsDisappearenceValue = -300;
            model.logElementsResetValue         = -100;

            model.chopLogMaxValue = 8;

            model.inputNames  = inputNames;
            model.inputValues = inputValues;
        end


        function allvarnames = getAllVarNames(model)
            chemsys = model.chemicalSystem;

            allvarnames = {'elements', 'logElements', 'presentElements', ...
                           'primvarElements', 'species', 'solidSpecies'};
            allvarnames = horzcat(allvarnames, chemsys.elementNames);
            allvarnames = horzcat(allvarnames, chemsys.logElementNames);
            allvarnames = horzcat(allvarnames, model.primvarElementsNames);
            allvarnames = horzcat(allvarnames, chemsys.speciesNames);
            allvarnames = horzcat(allvarnames, chemsys.solidNames);
        end

        function model = setPrimaryVarNames(model)
            primvarElementsNames   = model.primvarElementsNames;
            model.primaryVarNames = primvarElementsNames;
        end

        function state = syncPrimaryFromState(model, state)
            chemsys = model.chemicalSystem;
            nMC     = chemsys.nMC;

            presentElements = model.getProp(state, 'presentElements');
            elements        = model.getProp(state, 'elements');

            primvarElements = elements;
            for i = 1 : nMC
                presentElement = presentElements(:,  i);
                primvarElements(presentElement, i) = log(elements(presentElement, i));

                primvarElements(~presentElement, i) = 0;
            end
            
            state = model.setProp(state, 'primvarElements', primvarElements);

        end

        function state = syncStateFromPrimary(model, state)
            chemsys = model.chemicalSystem;
            nMC      = chemsys.nMC;

            presentElements = model.getProp(state, 'presentElements');
            primvarElements = model.getProp(state, 'primvarElements');

            elements    = primvarElements;
            logElements = primvarElements;

            for i = 1 : nMC
                presentElement = presentElements(:, i);
                elements(presentElement, i) = exp(primvarElements(presentElement, ...
                                                                  i));
                logElements(~presentElement, i) = NaN;
            end

            state = model.setProp(state, 'elements', elements);
            state = model.setProp(state, 'logElements', logElements);

        end

        function stateAD = syncStateADFromPrimary(model, stateAD)
            chemsys = model.chemicalSystem;
            nMC      = chemsys.nMC;

            presentElements = model.getADProp(stateAD, 'presentElements');
            primvarElements = model.getADProp(stateAD, 'primvarElements');

            for i = 1 : nMC
                presentElement = presentElements{i};

                elements{i}    = primvarElements{i};
                logElements{i} = primvarElements{i};

                elements{i}(presentElement) = exp(primvarElements{i}(presentElement));

                logElements{i}(~presentElement) = NaN;
            end

            stateAD = model.setADProp(stateAD, 'elements', elements);
            stateAD = model.setADProp(stateAD, 'logElements', logElements);
        end


        function state = syncPresentFlagsFromState(model, state)
            state = model.syncPresentElementsFromState(state);
        end


        function state = syncPresentElementsFromState(model, state)

            elements         = model.getProp(state, 'Elements');
            logElements      = model.getProp(state, 'logElements');

            ldisval = model.logElementsDisappearenceValue;
            disval = exp(ldisval);
            presentElements = (elements > disval);
            elements(~presentElements)    = 0;
            logElements(presentElements)  = log(elements(presentElements));
            logElements(~presentElements) = NaN;

            state = model.setProp(state, 'presentElements', presentElements);
            state = model.setProp(state, 'logElements'    , logElements);
            state = model.setProp(state, 'elements'       , elements);

        end

        function [problem, state] = getEquations(model, state0, state, dt, drivingForces, ...
                                                        varargin)
            error('not meant to be used')
        end
        
        function state = applyPhysicalConstraints(model, state)
            elements = model.getProp(state, 'elements');
            maxval   = exp(model.chopLogMaxValue);
            elements = min(elements, maxval);
            elements = max(elements, 0);
            state = model.setProp(state, 'elements', elements);
            state = model.syncPresentElementsFromState(state);
        end
            

        function [state, report] = updateState(model, state, problem, dx, ...
                                               drivingForces) %#ok
            [state, report] = updateState@PhysicalModel(model, state, problem, ...
                                                        dx, drivingForces);
            error('not updated')
            state = model.syncPresentElementsFromState(state);

        end

        function [fn, index] = getVariableField(model, name, varargin)

            chemsys  = model.chemicalSystem;
            varfound = false;
            failed   = false;

            while ~varfound

                if strcmpi(name, 'species')
                    varfound = true;
                    fn = 'species';
                    index = ':';
                    break
                end

                if strcmpi(name, 'elements')
                    varfound = true;
                    fn = 'elements';
                    index = ':';
                    break
                end

                if strcmpi(name, 'logElements')
                    varfound = true;
                    fn = 'logElements';
                    index = ':';
                    break
                end

                if strcmpi(name, 'presentElements')
                    varfound = true;
                    fn = 'presentElements';
                    index = ':';
                    break
                end

                if strcmpi(name, 'primvarElements')
                    varfound = true;
                    fn = 'primvarElements';
                    index = ':';
                    break
                end

                ind = strcmpi(name, model.primvarElementsNames);
                if any(ind)
                    varfound = true;
                    fn = 'primvarElements';
                    index = find(ind);
                    break
                end

                ind = strcmpi(name, chemsys.speciesNames);
                if any(ind)
                    varfound = true;
                    fn = 'species';
                    index = find(ind);
                    break
                end

                ind = strcmpi(name, chemsys.elementNames);
                if any(ind)
                    varfound = true;
                    fn = 'elements';
                    index = find(ind);
                    break
                end

                ind = strcmpi(name, chemsys.logElementNames);
                if any(ind)
                    varfound = true;
                    fn = 'logElements';
                    index = find(ind);
                    break
                end

                if strcmpi(name, 'solidSpecies')
                    varfound = true;
                    fn = 'solidSpecies';
                    index = ':';
                    break
                end

                ind = strcmpi(name, chemsys.solidNames);
                if any(ind)
                    varfound = true;
                    fn = 'solidSpecies';
                    index = find(ind);
                    break
                end
                
                varfound = true; % to exit the loop
                failed   = true;

            end

            if failed
                [fn, index] = deal([]);
            end

        end

    end
end



%{
Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
