classdef AqueousReactionModel < ChemicalBaseModel
% Model uses only composition-reaction equations 
% the total concentrations are given - to be used in transport.
% Ignore activity coefficients.
    
    properties
        primvarSpeciesNames
        
        chopLogMaxValue % chop max concentration value of concentration
                        % (log(c) < chopmaxvalue)
        
        % log-value for detection of disparition of species, see function
        % syncPresentSolidsAndSatindsFromState
        logSpeciesDisappearenceValue;
        logSpeciesResetValue;
    
        % regularization constant for the log
        regconstlog;
    end
    
    
    methods
        
        function model = AqueousReactionModel(chemsys, varargin)
            model = model@ChemicalBaseModel(chemsys, varargin{:});
            
            speciesNames = chemsys.speciesNames;
            nC           = chemsys.nC;

            primvarSpeciesNames = cell(1, nC);
            for i = 1 : nC
                primvarSpeciesNames{i} = strcat(speciesNames{i}, '_pvar');
            end
            model.primvarSpeciesNames   = primvarSpeciesNames;

            model.chopLogMaxValue = 10;
            model.logSpeciesDisappearenceValue = -40;
            model.logSpeciesResetValue         = -50;
            
            model.regconstlog = -inf;
            % model.regconstlog = 0;
        end

        function model = setPrimaryVarNames(model, state)
            primvarSpeciesNames   = model.primvarSpeciesNames;
            model.primaryVarNames = primvarSpeciesNames;
        end

        function allvarnames = getAllVarNames(model)
            chemsys  = model.chemicalSystem;

            allvarnames = {'elements', 'logElements', 'presentElements', ...
                           'species', 'logSpecies', 'presentSpecies', ...
                           'primvarSpecies'};
            
            allvarnames = horzcat(allvarnames, chemsys.elementNames);
            allvarnames = horzcat(allvarnames, chemsys.logElementNames);
            allvarnames = horzcat(allvarnames, chemsys.speciesNames);
            allvarnames = horzcat(allvarnames, chemsys.logSpeciesNames);
            allvarnames = horzcat(allvarnames, model.primvarSpeciesNames);

        end
        
        function state = syncStateFromPrimary(model, state)
            chemsys = model.chemicalSystem;
            nC      = chemsys.nC;

            presentSpecies = model.getProp(state, 'presentSpecies');
            primvarSpecies = model.getProp(state, 'primvarSpecies');

            species    = primvarSpecies;
            logSpecies = primvarSpecies;

            for i = 1 : nC
                presentSpecie = presentSpecies(:,  i);
                species(presentSpecie, i) = exp(primvarSpecies(presentSpecie, ...
                                                               i));
                logSpecies(~presentSpecie, i) = NaN;
            end

            state = model.setProp(state, 'species', species);
            state = model.setProp(state, 'logSpecies', logSpecies);
            
        end
        
        function stateAD = syncStateADFromPrimary(model, stateAD)
            chemsys = model.chemicalSystem;
            nC      = chemsys.nC;

            presentSpecies = model.getADProp(stateAD, 'presentSpecies');
            primvarSpecies = model.getADProp(stateAD, 'primvarSpecies');


            for i = 1 : nC
                presentSpecie = presentSpecies{i};

                species{i}    = primvarSpecies{i};
                logSpecies{i} = primvarSpecies{i};
                
                species{i}(presentSpecie) = exp(primvarSpecies{i}(presentSpecie));

                logSpecies{i}(~presentSpecie) = NaN;
            end

            stateAD = model.setADProp(stateAD, 'species', species);
            stateAD = model.setADProp(stateAD, 'logSpecies', logSpecies);
        end

        function state = syncPrimaryFromState(model, state)
            chemsys = model.chemicalSystem;
            nC      = chemsys.nC;

            presentSpecies = model.getProp(state, 'presentSpecies');
            species        = model.getProp(state, 'species');

            primvarSpecies = species;
            for i = 1 : nC
                presentSpecie = presentSpecies(:,  i);
                primvarSpecies(presentSpecie, i) = log(species(presentSpecie, i));

                primvarSpecies(~presentSpecie, i) = 0;
            end

            state = model.setProp(state, 'primvarSpecies', primvarSpecies);
            
        end

        function state = syncPresentFlagsFromState(model, state)
            state = model.syncPresentSpeciesFromState(state);
        end
        
        function state = syncPresentSpeciesFromState(model, state)
        % The fields presentSpecies, species and logSpecies are updated consequently
            chemsys = model.chemicalSystem;
            nMC = chemsys.nMC;
            nC  = chemsys.nC;
            CM  = chemsys.compositionMatrix;
            
            species         = model.getProp(state, 'Species');
            logSpecies      = model.getProp(state, 'logSpecies');
            presentElements = model.getProp(state, 'presentElements');
            
            ldisval = model.logSpeciesDisappearenceValue;
            disval = exp(ldisval);
            presentSpecies = (species > disval);
            
            % Set to absent the species which contains an absent element
            ncell = size(species, 1);
            impossibleSpecies = zeros(ncell, nC);
            absentElements = ~presentElements;
            for i = 1 : nMC
                impossibleSpecies = impossibleSpecies + bsxfun(@times, CM(i, ...
                                                                  :), ...
                                                               absentElements(:, ...
                                                                  i));
            end
            impossibleSpecies = (impossibleSpecies > 0);            

            presentSpecies = presentSpecies & ~impossibleSpecies;
            
            species(~presentSpecies)    = 0;
            logSpecies(presentSpecies)  = log(species(presentSpecies));
            logSpecies(~presentSpecies) = NaN;

            state = model.setProp(state, 'presentSpecies', presentSpecies);
            state = model.setProp(state, 'logSpecies'    , logSpecies);
            state = model.setProp(state, 'Species'       , species);


        end
        
        function [problem, state] = getEquations(model, state0, state, dt, drivingForces, varargin)
            
            opt = struct('Verbose', mrstVerbose, ...
                         'reverseMode', false,...
                         'resOnly', false,...
                         'iteration', -1);  % Compatibility only
            opt = merge_options(opt, varargin{:});
            
            % Initialization of primary variables
            if opt.resOnly
                stateAD = convertToCell(state);
            else
                [model, state, stateAD]  =  model.initADState(state);
            end
            
            [eqs, names, types] = model.evaluateModelEquation(state0, stateAD, dt, drivingForces, varargin{:})
            pVars = model.getPrimaryVarNames();
            problem = LinearizedProblem(eqs, types, names, pVars, state, dt);

        end
        
        function [eqs, names, types] = evaluateModelEquation(model, state0, stateAD, dt, drivingForces, varargin)
            
            [eqs, names, types] = equationsAqueousReaction(model, stateAD);
            
        end
        
        function [state, report] = stepFunction(model, state, state0, dt, ...
                                                drivingForces, linsolver, ...
                                                nonlinsolver, iteration, ...
                                                varargin)

            state = detectEquilibriumNoSolid(model, state);

            [state, report] = stepFunction@PhysicalModel(model, state, state0, ...
                                                         dt, drivingForces, ...
                                                         linsolver, nonlinsolver, ...
                                                         iteration, ...
                                                         varargin{:});

        end

        function state = applyPhysicalConstraints(model, state)
            chemsys = model.chemicalSystem;
            nC = chemsys.nC;
            
            elements = model.getProp(state, 'elements');
            species = model.getProp(state, 'species');
            for i = 1 : nC
                maxvals = chemsys.maxMatrices{i}*(elements');
                maxvals = (min(maxvals))';
                species(:, i) = min(species(:, i), maxvals);
                species(:, i) = max(species(:, i), 0);
            end
            state = model.setProp(state, 'species', species);
            state = model.syncPresentSpeciesFromState(state);
        end
            
        function [state, report] = updateState(model, state, problem, dx, drivingForces) %#ok
        % Update state based on Newton increments

            model = model.setPrimaryVarNames(); % Primary variables are lost due to
                                                % simulateSchedule mechanism

            [state, report] = updateState@PhysicalModel(model, state, problem, ...
                                                        dx, drivingForces);
            state = model.syncStateFromPrimary(state);
            state = model.applyPhysicalConstraints(state);
            state = model.syncPrimaryFromState(state);
            
            report = [];
        end
        
        function [state, report] = updateAfterConvergence(model, state0, state, ...
                                                          dt, drivingForces)
            [state, report] = updateAfterConvergence@ChemicalBaseModel(model, ...
                                                              state0, state, ...
                                                              dt, ...
                                                              drivingForces);
            state = model.syncPresentFlagsFromState(state);

            if model.debugmode
                debugPlot(model, state);
            end
        end
        
        function state = setupInitialGuess(model, userInput)

            nI = size(userInput, 1);

            chemsys = model.chemicalSystem;
            nC  = chemsys.nC;
            nMC = chemsys.nMC;

            % default values
            elements        = ones(nI, nMC)*mol/litre;
            presentElements = false(nI, nMC);
            species         = ones(nI, nC)*mol/litre;
            presentSpecies  = false(nI, nC);

            state = [];
            state = model.setProp(state, 'presentElements', presentElements);
            state = model.setProp(state, 'elements'       , elements);
            state = model.setProp(state, 'presentSpecies' , presentSpecies);
            state = model.setProp(state, 'species'        , species);
            
            inputNames = chemsys.inputNames;
            for i = 1 : numel(inputNames)
                state = model.setProp(state, inputNames{i}, userInput(:, i));
            end
            elements = model.getProp(state, 'elements');
            presentElements(elements > 0) = true;
            logElements = NaN(nI, nMC);
            logElements(presentElements) = log(elements(presentElements));
 
            species = model.getProp(state, 'species');
            presentSpecies(species > 0) = true;
            logSpecies = NaN(nI, nC);
            logSpecies(presentSpecies) = log(species(presentSpecies));

            state = [];
            state = model.setProp(state, 'presentElements', presentElements);
            state = model.setProp(state, 'elements'       , elements);
            state = model.setProp(state, 'logElements'    , logElements);
            state = model.setProp(state, 'presentSpecies' , presentSpecies);
            state = model.setProp(state, 'species'        , species);
            state = model.setProp(state, 'logSpecies'     , logSpecies);

        end
    
        function [state, report] = initState(model, userInput, varargin)
            
            opt = struct('chargeBalance', []);
            opt = merge_options(opt, varargin{:});
            chargeBalance  = opt.chargeBalance;
            
            stateguess = model.setupInitialGuess(userInput);
            chemsys    = model.chemicalSystem;
            inputNames = chemsys.inputNames;
            [state, failure, report] = model.solveChemicalState(inputNames, ...
                                                              userInput, ...
                                                              stateguess, ...
                                                              'chargeBalance', ...
                                                              chargeBalance);
            if failure
                warning('initState function did not converge');
            end
            
        end
        
        function cheminputmodel = getChemInputModel(model, inputmodel)
            cheminputmodel = AqueousReactionInputModel(model, inputmodel);
        end
        
        function cheminputmodel = getChemInputCharBalModel(model, inputmodel)
            cheminputmodel = AqueousReactionInputChargeBalanceModel(model, inputmodel);
        end

        function [state, failure, report] = solveChemicalState(model, ...
                                                              inputNames, ...
                                                              inputValues, ...
                                                              stateguess, varargin)

            opt = struct('chargeBalance', []);
            opt = merge_options(opt, varargin{:});
            
            chemsys = model.chemicalSystem;
            inputmodel = InputLogModel(chemsys, inputNames, inputValues);                

            chemmodel = model;
            
            cheminputmodel = getChemInputModel(chemmodel, inputmodel);
            cheminputmodel.nonLinearMaxIterations = chemmodel.nonLinearMaxIterations;
            cheminputmodel.verbose                = chemmodel.verbose;
            cheminputmodel.nonlinearTolerance     = chemmodel.nonlinearTolerance;
            
            % We change the default values such that they fulfill the natural bounds.
            stateguess = chemmodel.applyPhysicalConstraints(stateguess);
            % We udpate the primary variables from the state.
            stateguess = cheminputmodel.syncPrimaryFromState(stateguess);

            %% solve the chemical system
            [state, failure, report] = cheminputmodel.solveChemicalState(stateguess);
        
            if ~isempty(opt.chargeBalance)
                
                fprintf('Solving the chemical system with strict charge balance...\n');
                
                CVC = opt.chargeBalance;
                % we modify input model
                inputNames  = inputmodel.inputNames;
                inputValues = inputmodel.inputValues;
                
                ind = ismember(CVC, inputNames);
                inputmodel.inputNames = inputNames(~ind);
                inputmodel.inputValues = inputNames(~ind);
                
                cheminputmodel = getChemInputModel(chemmodel, inputmodel);
                cheminputmodel.nonLinearMaxIterations = chemmodel.nonLinearMaxIterations;
                cheminputmodel.verbose                = chemmodel.verbose;
                cheminputmodel.nonlinearTolerance     = chemmodel.nonlinearTolerance;                
                
                [state, failure, report] = cheminputmodel.solveChemicalState(state);                
                
            end
            
        end
        
        
        
        function [fn, index] = getVariableField(model, name, varargin)

            chemsys  = model.chemicalSystem;
            varfound = false;
            failed   = false;

            while ~varfound

                if strcmpi(name, 'elements')
                    varfound = true;
                    fn = 'elements';
                    index = ':';
                    break
                end

                if strcmpi(name, 'logElements')
                    varfound = true;
                    fn = 'logElements';
                    index = ':';
                    break
                end

                if strcmpi(name, 'presentElements')
                    varfound = true;
                    fn = 'presentElements';
                    index = ':';
                    break
                end

                if strcmpi(name, 'species')
                    varfound = true;
                    fn = 'species';
                    index = ':';
                    break
                end

                if strcmpi(name, 'logSpecies')
                    varfound = true;
                    fn = 'logSpecies';
                    index = ':';
                    break
                end

                if strcmpi(name, 'presentSpecies')
                    varfound = true;
                    fn = 'presentSpecies';
                    index = ':';
                    break
                end
                
                if strcmpi(name, 'primvarSpecies')
                    varfound = true;
                    fn = 'primvarSpecies';
                    index = ':';
                    break
                end

                ind = strcmpi(name, chemsys.elementNames);
                if any(ind)
                    varfound = true;
                    fn = 'elements';
                    index = find(ind);
                    break
                end

                ind = strcmpi(name, chemsys.logElementNames);
                if any(ind)
                    varfound = true;
                    fn = 'logElements';
                    index = find(ind);
                    break
                end

                ind = strcmpi(name, chemsys.speciesNames);
                if any(ind)
                    varfound = true;
                    fn = 'species';
                    index = find(ind);
                    break
                end


                ind = strcmpi(name, chemsys.logSpeciesNames);
                if any(ind)
                    varfound = true;
                    fn = 'logSpecies';
                    index = find(ind);
                    break
                end

                ind = strcmpi(name, model.primvarSpeciesNames);
                if any(ind)
                    varfound = true;
                    fn = 'primvarSpecies';
                    index = find(ind);
                    break
                end
                

                varfound = true; % to exit the loop
                failed   = true;
            end

            if failed
                [fn, index] = deal([]);
            end

        end

        
    end

end
    


%{
Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
