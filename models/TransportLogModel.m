classdef TransportLogModel < TransportModel


    methods

        function model = TransportLogModel(G, rock, fluid, bc, chemsys)
            model = model@TransportModel(G, rock, fluid, bc, chemsys);
        end

        function state = syncPrimaryFromState(model, state)
            chemsys = model.chemicalSystem;
            nMC     = chemsys.nMC;

            presentElements = model.getProp(state, 'presentElements');
            elements        = model.getProp(state, 'elements');

            primvarElements = elements;
            for i = 1 : nMC
                presentElement = presentElements(:,  i);
                primvarElements(presentElement, i) = log(elements(presentElement, i));

                primvarElements(~presentElement, i) = 0;
            end

            state = model.setProp(state, 'primvarElements', primvarElements);

        end

        function state = syncStateFromPrimary(model, state)
            chemsys = model.chemicalSystem;
            nMC      = chemsys.nMC;

            presentElements = model.getProp(state, 'presentElements');
            primvarElements = model.getProp(state, 'primvarElements');

            elements    = primvarElements;
            logElements = primvarElements;

            for i = 1 : nMC
                presentElement = presentElements(:,  i);
                elements(presentElement, i) = exp(primvarElements(presentElement, ...
                                                                  i));
                logElements(~presentElement, i) = NaN;
            end

            state = model.setProp(state, 'elements', elements);
            state = model.setProp(state, 'logElements', logElements);

        end

        function stateAD = syncStateADFromPrimary(model, stateAD)
            chemsys = model.chemicalSystem;
            nMC      = chemsys.nMC;

            presentElements = model.getADProp(stateAD, 'presentElements');
            primvarElements = model.getADProp(stateAD, 'primvarElements');

            for i = 1 : nMC
                presentElement = presentElements{i};

                elements{i}    = primvarElements{i};
                logElements{i} = primvarElements{i};

                elements{i}(presentElement) = exp(primvarElements{i}(presentElement));

                logElements{i}(~presentElement) = NaN;
            end

            stateAD = model.setADProp(stateAD, 'elements', elements);
            stateAD = model.setADProp(stateAD, 'logElements', logElements);
        end
    
    end

end



%{
Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
