classdef DissPrecModel < AqueousReactionModel
% Model for precipitation/dissolution to be used with transport
%
% The implementation is not optimal as it should rather be derived from
% DissPrecModel. We will try to fix this unconsistency later.

    properties
        % The primary variable for a solid component is either the
        % concentration (when the solid is completely dissolved and the value
        % is then equal to zero) or the log
        % of the concentration (the log is appropriate to get good
        % convergence propertied).
        % We introduce a new variable name for this primary variable.
        % We have one such variable for each solid specie.
        primvarSolidNames;

        % log-value for detection of total dissolution, see function
        % syncPresentSolidsFromState
        logSolidsDisappearenceValue;
        logSolidsResetValue;

    end

    methods

        function model = DissPrecModel(chemsys, varargin)
            model = model@AqueousReactionModel(chemsys, varargin{:});

            chemsys    = model.chemicalSystem;
            solidNames = chemsys.solidNames;
            nS         = chemsys.nS;

            primvarSolidNames = cell(1, nS);
            for i = 1 : nS
                primvarSolidNames{i} = strcat(solidNames{i}, '_pvar');
            end
            model.primvarSolidNames = primvarSolidNames;

            lsd = model.logSpeciesDisappearenceValue;
            lsr = model.logSpeciesResetValue;
            model.logSolidsDisappearenceValue = -30;
            model.logSolidsResetValue         = -40;
        end

        function allvarnames = getAllVarNames(model)

            chemsys  = model.chemicalSystem;

            allvarnames = getAllVarNames@AqueousReactionModel(model);

            allvarnames = horzcat(allvarnames                    , ...
                                  chemsys.solidNames             , ...
                                  chemsys.logSolidNames          , ...
                                  model.primvarSolidNames);

            allvarname = horzcat(allvarnames, {'presentSolids', 'solidSpecies', ...
                                'logSolidSpecies'});

        end

        function model = setPrimaryVarNames(model)
            model = setPrimaryVarNames@AqueousReactionModel(model);
            primaryVarNames = model.primaryVarNames;

            model.primaryVarNames = horzcat(primaryVarNames, ...
                                            model.primvarSolidNames);
        end

        function state = syncStateFromPrimary(model, state)
        % Use values of the primary variables to synchronize the other
        % variables in the system

            % sync variables from AqueousReactionModel
            state = syncStateFromPrimary@AqueousReactionModel(model, state);

            chemsys = model.chemicalSystem;
            nS      = chemsys.nS;
            % sync 'solidSpecies' and 'logSolidSpecies'
            presentSolids      = model.getProp(state, 'presentSolids');
            primvarSolids      = model.getProp(state, 'primvarSolids');
            solidComponents    = primvarSolids;
            logSolidComponents = primvarSolids;

            for i = 1 : nS
                presentSolid = presentSolids(:, i);

                solidComponents(presentSolid, i) = ...
                    exp(logSolidComponents(presentSolid, i));
                logSolidComponents(~presentSolid, i) = NaN;
            end
            state = model.setProp(state, 'solidSpecies'   , solidComponents);
            state = model.setProp(state, 'logSolidSpecies', logSolidComponents);
        end

        function stateAD = syncStateADFromPrimary(model, stateAD)
        % Use values of the primary variables to synchronize the other
        % variables in the system

            % sync variables from AqueousReactionModel
            stateAD = syncStateADFromPrimary@AqueousReactionModel(model, ...
                                                              stateAD);

            chemsys = model.chemicalSystem;
            nS      = chemsys.nS;

            % sync 'solidSpecies' and 'logSolidSpecies'
            presentSolids = model.getADProp(stateAD, 'presentSolids');
            primvarSolids = model.getADProp(stateAD, 'primvarSolids');

            for i = 1 : nS
                presentSolid = presentSolids{i};

                solidComponents{i}    = primvarSolids{i};
                logSolidComponents{i} = primvarSolids{i};

                solidComponents{i}(presentSolid)    = ...
                    exp(logSolidComponents{i}(presentSolid));

                logSolidComponents{i}(~presentSolid) = NaN;

            end
            stateAD = model.setADProp(stateAD, 'solidSpecies'   , solidComponents);
            stateAD = model.setADProp(stateAD, 'logSolidSpecies', logSolidComponents);

        end

        function state = syncPresentFlagsFromState(model, state)
            state = syncPresentFlagsFromState@AqueousReactionModel(model, ...
                                                              state);
            state = model.syncPresentSolidsFromState(state);
        end

        function state = syncPresentSolidsFromState(model, state)

            % We detect dissolution and precipitation and update state
            % consequently

            presentSolids    = model.getProp(state, 'presentSolids');
            solidSpecies     = model.getProp(state, 'solidSpecies');
            logSolidSpecies  = model.getProp(state, 'logSolidSpecies');

            chemsys   = model.chemicalSystem;
            nS        = chemsys.nS;
            ldisval   = model.logSolidsDisappearenceValue;
            disval    = exp(ldisval);

            for i = 1 : nS
                ps   = presentSolids(:, i);
                ss   = solidSpecies(:, i);
                lss  = logSolidSpecies(:, i);

                % solid is present but very low value -> we set solid as absent
                ind = ps & (ss < disval);

                ps(ind)  = false;
                ss(ind)  = 0;
                lss(ind) = NaN;
                
                % We reset the values of the non present solids to zero
                % detectEquilibrium is in charge alone to revive the species.
                ss(~ps) = 0;
                lss(~ps) = NaN;

                presentSolids(:, i)   = ps;
                solidSpecies(:, i)    = ss;
                logSolidSpecies(:, i) = lss;

            end

            % Set to absent the solids which contains an absent element
            presentElements = model.getProp(state, 'presentElements');
            chemsys = model.chemicalSystem;
            nMC = chemsys.nMC;
            nS  = chemsys.nS;
            SCM = chemsys.solidContributionMatrix;
            ncell = size(solidSpecies, 1);
            impossibleSolids = zeros(ncell, nS);
            absentElements = ~presentElements;
            for i = 1 : nMC
                impossibleSolids = impossibleSolids + bsxfun(@times, SCM(i, ...
                                                                  :), ...
                                                               absentElements(:, ...
                                                                  i));
            end
            impossibleSolids = (impossibleSolids > 0);            
            presentSolids(impossibleSolids)   = 0;
            solidSpecies(impossibleSolids)    = 0;
            logSolidSpecies(impossibleSolids) = NaN;
            
            state = model.setProp(state, 'presentSolids'        , presentSolids);
            state = model.setProp(state, 'solidSpecies'         , solidSpecies);
            state = model.setProp(state, 'logSolidSpecies'      , logSolidSpecies);

        end


        function state = syncPrimaryFromState(model, state)

            state = syncPrimaryFromState@ ...
                    AqueousReactionModel(model, state);

            % sync the solid primary variable
            nS              = model.chemicalSystem.nS;
            presentSolids   = model.getProp(state, 'presentSolids');
            solidSpecies    = model.getProp(state, 'solidSpecies');
            logSolidSpecies = model.getProp(state, 'logSolidSpecies');

            primvarSolids = solidSpecies;
            for i = 1 : nS
                presentSolid = presentSolids(:, i);
                primvarSolids(presentSolid, i) = logSolidSpecies(presentSolid, i);
            end
            state = model.setProp(state, 'primvarSolids', primvarSolids);
        end


        function [eqs, names, types] = evaluateModelEquation(model, state0, stateAD, dt, drivingForces, varargin)
            
            [eqs, names, types] = equationsDissPrecipitation(model, stateAD);
            
        end

        function [state, report] = stepFunction(model, state, state0, dt, ...
                                                drivingForces, linsolver, ...
                                                nonlinsolver, iteration, ...
                                                varargin)

            state = detectEquilibrium(model, state);

            [state, report] = stepFunction@PhysicalModel(model, state, state0, ...
                                                         dt, drivingForces, ...
                                                         linsolver, nonlinsolver, ...
                                                         iteration, ...
                                                         varargin{:});

        end

        function state = applyPhysicalConstraints(model, state)
            state = model.applyPhysicalConstraints@AqueousReactionModel(state);
            
            chemsys = model.chemicalSystem;
            nS = chemsys.nS;
            
            elements = model.getProp(state, 'elements');
            solids = model.getProp(state, 'solidSpecies');
            logSolids = model.getProp(state, 'logSolidSpecies');

            for i = 1 : nS
                maxvals = chemsys.maxSolidMatrices{i}*(elements');
                maxvals = (min(maxvals))';
                solids(:, i) = min(solids(:, i), maxvals);
                solids(:, i) = max(solids(:, i), 0);
            end
            % update log variables
            presentSolids = (solids > 0);
            logSolids(presentSolids)  = log(solids(presentSolids));
            logSolids(~presentSolids) = NaN;
            
            state = model.setProp(state, 'solidSpecies', solids);
            state = model.setProp(state, 'logSolidSpecies', logSolids);
            state = model.syncPresentSolidsFromState(state);
        end
        
        function [state, report] = updateState(model, state, problem, dx, drivingForces) %#ok
        % Update state based on Newton increments

            [state, report] = updateState@AqueousReactionModel(model, state, ...
                                                              problem, dx, ...
                                                              drivingForces);
            if model.debugmode
                debugPlot(model, state);
            end

            report = [];
        end

        function state = setupInitialGuess(model, userInput)
            state = setupInitialGuess@AqueousReactionModel(model, userInput);

            nI = size(userInput, 1);
            chemsys = model.chemicalSystem;
            nS  = chemsys.nS;

            solidSpecies    = ones(nI, nS)*mol/litre;
            state = model.setProp(state, 'solidSpecies', solidSpecies);
            inputNames = chemsys.inputNames;
            for i = 1 : numel(inputNames)
                state = model.setProp(state, inputNames{i}, userInput(:, i));
            end
            solidSpecies = model.getProp(state, 'solidSpecies');

            presentSolids = false(nI, nS);
            presentSolids(solidSpecies > 0) = true;
            
            logSolidSpecies = NaN(nI, nS);
            logSolidSpecies(presentSolids) = log(solidSpecies(presentSolids));
            
            state = model.setProp(state, 'presentSolids'  , presentSolids);
            state = model.setProp(state, 'solidSpecies'   , solidSpecies);
            state = model.setProp(state, 'logSolidSpecies', logSolidSpecies);

        end
        
        function cheminputmodel = getChemInputModel(model, inputmodel)
            cheminputmodel = DissPrecInputModel(model, inputmodel);
        end
               
        
        function [state, failure, report] = solveChemicalState(model, inputNames, ...
                                                              inputValues, ...
                                                              stateguess, varargin)
            
            [state, failure, report] = solveChemicalState@AqueousReactionModel(model, ...
                                                              inputNames, ...
                                                              inputValues, ...
                                                              stateguess, ...
                                                              varargin{:});
        end
        
        function [fn, index] = getVariableField(model, name, varargin)

            [fn, index] = getVariableField@ ...
                AqueousReactionModel(model, name);
            chemsys  = model.chemicalSystem;

            if ~isempty(index)
                return
            else
                varfound = false;
                failed   = false;
            end

            while ~varfound

                if strcmpi(name, 'solidSpecies')
                    varfound = true;
                    fn = 'solidSpecies';
                    index = ':';
                    break
                end

                ind = strcmpi(name, chemsys.solidNames);
                if any(ind)
                    varfound = true;
                    fn = 'solidSpecies';
                    index = find(ind);
                    break
                end

                if strcmpi(name, 'logSolidSpecies')
                    varfound = true;
                    fn = 'logSolidSpecies';
                    index = ':';
                    break
                end

                ind = strcmpi(name, chemsys.logSolidNames);
                if any(ind)
                    varfound = true;
                    fn = 'logSolidSpecies';
                    index = find(ind);
                    break
                end

                if strcmpi(name, 'presentSolids')
                    varfound = true;
                    fn = 'presentSolids';
                    index = ':';
                    break
                end

                if strcmpi(name, 'primvarSolids')
                    varfound = true;
                    fn = 'primvarSolids';
                    index = ':';
                    break
                end

                ind = strcmpi(name, model.primvarSolidNames);
                if any(ind)
                    varfound = true;
                    fn = 'primvarSolids';
                    index = find(ind);
                    break
                end

                varfound = true; % to exit the loop
                failed   = true;
            end

            if failed
                [fn, index] = deal([]);
            end

        end

    end


end



%{
Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
