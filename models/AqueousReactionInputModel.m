classdef AqueousReactionInputModel < CompositeModel

    properties
        plotIterations         % toggle plotting of iteration information true or false
        nonLinearMinIterations % minimum number of iterations for the nonlinear solver
        nonLinearMaxIterations % maximum number of iterations for the nonlinear solver
        linearTolerance        % tolerance of the residual of the linear system, for backslash
        linearMaxIterations    % maximum number of iterations for the linear solver
    end
    
    methods

        function model = AqueousReactionInputModel(chemmodel, inputmodel)
            
            model.SubModels = {chemmodel, inputmodel};
            model.SubModelNames = {'chemicalModel', 'inputModel'};
            
            model = model.setupCompositeModel();

            model.plotIterations         = false;
            model.nonLinearMinIterations = 1;
            model.nonLinearMaxIterations = 40;
            model.linearMaxIterations    = 25;
            model.linearTolerance        = 1e-8;
            model.debugmode = chemmodel.debugmode;
            
        end

        function [allvarnames, modelindex] = getAllVarNames(model)
            
            chemmodel  = model.getSubModel('chemicalModel');
            inputmodel = model.getSubModel('inputModel');
            
            chemvarnames = chemmodel.getAllVarNames();
            allvarnames = chemvarnames;
            n1 = numel(allvarnames);
            chemmodelindex  = true(n1, 1);
            inputmodelindex = false(n1, 1);

            inputvarnames = inputmodel.getAllVarNames();
            for i = 1 : numel(inputvarnames)
                inputvarname = inputvarnames{i};
                isInChemvars = strcmp(inputvarname, chemvarnames);
                if any(isInChemvars)
                    indvar = find(isInChemvars);
                    inputmodelindex(indvar) = true;
                else
                    allvarnames{end + 1} = inputvarname;
                end
            end

            n2 = numel(allvarnames);
            modelindex = false(n2, 2);
            modelindex(1 : n1, 1) = chemmodelindex;
            modelindex(1 : n1, 2) = inputmodelindex;
            modelindex((n1 + 1) : n2, 1) = false;
            modelindex((n1 + 1) : n2, 2) = true;
            
        end
        
        function state = syncStateFromPrimary(model, state)
            % We use the chemical model handle the synchronization
            chemmodel  = model.getSubModel('chemicalModel');
            state = chemmodel.syncStateFromPrimary(state);

            inputmodel = model.getSubModel('inputModel');
            state      = inputmodel.syncStateFromPrimary(state);
        end

        function stateAD = syncStateADFromPrimary(model, stateAD)
        % The AD version of syncStateFromPrimary

            % We use the chemical model handle the synchronization
            chemmodel  = model.getSubModel('chemicalModel');
            stateAD = chemmodel.syncStateADFromPrimary(stateAD);

            inputmodel = model.getSubModel('inputModel');
            stateAD    = inputmodel.syncStateADFromPrimary(stateAD);
        end
        
        function state = syncPresentFlagsFromState(model, state)
            
            inputmodel = model.getSubModel('inputModel');
            state = inputmodel.syncPresentFlagsFromState(state);
            
            chemmodel = model.getSubModel('chemicalModel');
            state = chemmodel.syncPresentFlagsFromState(state);
            
        end

        function state = applyPhysicalConstraints(model, state)
            inputmodel = model.getSubModel('inputModel');
            chemmodel  = model.getSubModel('chemicalModel');
            state = inputmodel.applyPhysicalConstraints(state);
            state = chemmodel.applyPhysicalConstraints(state);
        end
        
        
        function [problem, state] = getEquations(model, state0, state, dt, drivingForces, ...
                                                        varargin)

            opt = struct('Verbose'    , mrstVerbose, ...
                         'reverseMode', false      ,...
                         'resOnly'    , false      ,...
                         'iteration'  , -1);  % Compatibility only
            opt = merge_options(opt, varargin{:});


            % Initialization of primary variables
            if opt.resOnly
                stateAD = convertToCell(state);
            else
                [model, state, stateAD]  =  model.initADState(state);
            end

            chemmodel  = model.getSubModel('chemicalModel');
            inputmodel = model.getSubModel('inputModel');

            [chem_eqs, chem_names, chem_types] = equationsAqueousReaction(chemmodel, ...
                                                              stateAD);

            [input_eqs, input_names, input_types] = equationsInput(inputmodel, ...
                                                              stateAD);
            eqs   = horzcat(input_eqs, chem_eqs);
            names = {input_names{:}, chem_names{:}};
            types = {input_types{:}, chem_types{:}};
            pVars = model.getPrimaryVarNames();

            problem = LinearizedProblem(eqs, types, names, pVars, state, dt);

        end
        
        function state = setInputValues(model, state)
            inputmodel = model.getSubModel('inputModel');
            inputNames = inputmodel.inputNames;
            inputValues = inputmodel.inputValues;
            for i = 1 : numel(inputNames)
                state = model.setProp(state, inputNames{i}, inputValues(:, i));
            end
            state = syncPresentFlagsFromState(model, state);
        end
        

        function [state, report] = stepFunction(model, state, state0, dt, ...
                                                drivingForces, linsolver, ...
                                                nonlinsolver, iteration, varargin)

            chemmodel = model.getSubModel('chemicalModel');
            state = detectEquilibriumNoSolid(chemmodel, state);

            [state, report] = stepFunction@PhysicalModel(model, state, state0, ...
                                                         dt, drivingForces, ...
                                                         linsolver, nonlinsolver, ...
                                                         iteration, varargin{:});

        end

        function [state, report] = updateState(model, state, problem, dx, drivingForces) %#ok
        % Update state based on Newton increments

            model = model.setPrimaryVarNames(); % primary variables are lost due to
                                                % simulateSchedule mechanism

            [state, report] = updateState@PhysicalModel(model, state, problem, ...
                                                        dx, drivingForces);

            % update the other variables from the primary variables
            state = model.syncStateFromPrimary(state);
            state = model.applyPhysicalConstraints(state);
            state = model.setInputValues(state);
            state = model.syncPrimaryFromState(state);
            
            if model.debugmode
                chemmodel = model.getSubModel('chemicalModel');
                debugPlot(chemmodel, state);
            end
            
        end

        function [state, failure, report] = solveChemicalState(model, stateguess)
            solver = NonLinearSolver();

            solver.maxIterations              = model.nonLinearMaxIterations;
            solver.minIterations              = model.nonLinearMinIterations;
            solver.LinearSolver.tolerance     = model.linearTolerance;
            solver.LinearSolver.maxIterations = model.linearMaxIterations;

            dt            = 0; % dummy timestep
            drivingForces = []; % drivingForces;
            stateguess0   = stateguess;

            [state, failure, report] = solveMinistep(solver, model, stateguess, ...
                                                     stateguess0, dt, ...
                                                     drivingForces);
            if failure
                msg = ['Model step resulted in failure state. Reason: ', ...
                       report.NonlinearReport{end}.FailureMsg]; 
                error(msg);
            end
            

        end
    end
end



%{
Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
