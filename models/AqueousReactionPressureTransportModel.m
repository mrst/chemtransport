classdef AqueousReactionPressureTransportModel < ChemicalPressureTransportModel
        
    methods

        function model = AqueousReactionPressureTransportModel(chemmodel, ...
                                                             transportmodel)
            if ~(isa(chemmodel, 'AqueousReactionModel'))
                printf(['WARNING: this transport model is meant to be used with ' ...
                        'AqueousReactionModel. This is not ' ...
                        'the case here.']);
            end
            model = model@ChemicalPressureTransportModel(chemmodel, transportmodel);
        end

        
        function [state, report] = stepFunction(model, state, state0, dt, drivingForces, linsolver, nonlinsolver, iteration, varargin)


            if model.doFirstStepExplicit & (iteration == 1 )
                % Update element concentation with an explicit transport step
                transmodel = model.getSubModel('transportModel');
                state = transmodel.stepFunction(state, state0, dt, drivingForces, ...
                                                linsolver, nonlinsolver, 1);
                varsToBound = {'elements'};
                state = applyNaturalBounds(transmodel, varsToBound, state);
                
                % Update chemical system using the new element concentration
                chemmodel = model.getSubModel('chemicalModel');
                
                % Sync the variables before call of solveChemicalState
                varnames    = {'elements'};
                logvarnames = {'logElements'};
                state = syncLogVariable(model, state, varnames, logvarnames);
                [state, failure, report] = chemmodel.solveChemicalState(state);
            end
            
            % Proceed with an fully-implicit step
            [state, report] = stepFunction@PhysicalModel(model, state, state0, ...
                                                         dt, drivingForces, ...
                                                         linsolver, nonlinsolver, ...
                                                         iteration, varargin{:});

            if iteration < model.exactChemSolveIterations 
                error('code not checked yet')
                % Update chemical system using the new element concentration
                chemmodel = model.getSubModel('chemicalModel');
                [state, failure, chemreport] = chemmodel.solveChemicalState(state);
                % We do not handle the case where this step fails for now. It is thus safer to
                % throw an error.
                assert(chemreport.Converged, 'internal chemical state computation did not converge')
            end
        end

        

        function [state, report] = updateState(model, state, problem, dx, drivingForces) %#ok
        % Update state based on Newton increments
            
            model = model.setPrimaryVarNames(); % primary variables are lost due to
                                                % simulateSchedule mechanism...
            
            [state, report] = updateState@PhysicalModel(model, state, problem, ...
                                                        dx, drivingForces);

            % update the other variables from the primary variables
            state = model.syncStateFromPrimary(state);
            
            % We apply the natural bounds for chemical systems using the
            % chemical model : 
            % Important : The order matters, 'elements' shoud be first
            chemmodel = model.getSubModel('chemicalModel');
            varsToBound = {'elements', 'species'};
            state = applyNaturalBounds(chemmodel, varsToBound, state);
            % we need to sync the log for 'elements' 
            varnames    = {'elements'};
            logvarnames = {'logElements'};
            state = syncLogVariable(model, state, varnames, logvarnames);
            
            state = model.syncPresentFlagsFromState(state);
            state = model.syncPrimaryFromState(state);
            
            if model.debugmode
                debugPlot(chemmodel, state);
            end
            
        end
        
    end
end




%{
Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
