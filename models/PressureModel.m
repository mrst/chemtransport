classdef PressureModel < SimpleModel

    properties
        rock
        fluid
        chemicalSystem

        % boundary conditions: The bc structure contains the following fields
        % 'dirCell'     : Cell where pressure is imposed
        % 'dirPressure' : Value of the pressure        
        % 'influxcells' : Cells where injection takes place
        % 'influxrates' : Injection rate
        bc

        nonLinearMinIterations % minimum number of iterations for the nonlinear solver
        nonLinearMaxIterations % maximum number of iterations for the nonlinear solver
        linearTolerance        % tolerance of the residual of the linear system, for backslash
        linearMaxIterations    % maximum number of iterations for the linear
                               % solver
        
    end

    methods

        function model = PressureModel(G, rock, fluid, bc, chemsys)
            model      = model@SimpleModel();

            model.G     = G;
            model.rock  = rock;
            model.fluid = fluid;
            model.bc    = bc;
            model.chemicalSystem = chemsys;

            model.operators = setupOperatorsTPFA(G, rock);
        
            model.nonLinearMinIterations    = 1;
            model.nonLinearMaxIterations    = 40;
            model.linearMaxIterations       = 25;
            model.linearTolerance           = 1e-8;

        end

        function model = setPrimaryVarNames(model, state)
            model.primaryVarNames = {'pressure'};
        end
        
        function allvarnames = getAllVarNames(model)
            chemsys = model.chemicalSystem;

            allvarnames = {'solidSpecies'};
            allvarnames = horzcat(allvarnames, chemsys.solidNames);
            allvarnames = horzcat(allvarnames, {'pressure', 'p'});
        end


        function [problem, state] = getEquations(model, state0, state, dt, drivingForces, ...
                                                        varargin)
            opt = struct('Verbose', mrstVerbose, ...
                         'reverseMode', false,...
                         'resOnly', false,...
                         'iteration', -1);  % Compatibility only
            opt = merge_options(opt, varargin{:});
            
            % Initialization of primary variables
            if opt.resOnly
                stateAD = convertToCell(state);
            else
                [model, state, stateAD]  =  model.initADState(state);
            end
            state0 = convertToCell(state0);
            [eqs, names, types] = model.evaluateModelEquation(state0, stateAD, ...
                                                              dt, drivingForces, ...
                                                              varargin{:});
            pVars = model.getPrimaryVarNames();
            problem = LinearizedProblem(eqs, types, names, pVars, state, dt);

        end
        
        function [eqs, names, types] = evaluateModelEquation(model, state0, stateAD, dt, drivingForces, varargin)
            
            [eqs, names, types] = equationsPressure(state0, stateAD, model, ...
                                                    dt, drivingForces, ...
                                                    varargin{:});
        end
        
        function [state, report] = updateState(model, state, problem, dx, ...
                                               drivingForces) %#ok
            [state, report] = updateState@PhysicalModel(model, state, problem, ...
                                                        dx, drivingForces);
            error('add bound here');
        end

        function [fn, index] = getVariableField(model, name, varargin)

            chemsys  = model.chemicalSystem;
            varfound = false;
            failed   = false;

            while ~varfound
                if strcmpi(name, 'solidSpecies')
                    varfound = true;
                    fn = 'solidSpecies';
                    index = ':';
                    break
                end

                ind = strcmpi(name, chemsys.solidNames);
                if any(ind)
                    varfound = true;
                    fn = 'solidSpecies';
                    index = find(ind);
                    break
                end
                
                if any(strcmpi(name, {'p', 'pressure'}))
                    fn    = 'pressure';
                    index = 1;
                    break;
                end

                varfound = true; % to exit the loop
                failed   = true;

            end

            if failed
                [fn, index] = deal([]);
            end

        end

    end
end



%{
Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
